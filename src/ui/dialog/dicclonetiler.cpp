/*
 * dicclonetiler.cpp
 *
 *  Created on: 08-May-2019
 *      Author: Vishal Virmani
 */


#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include "dicclonetiler.h"

#include <climits>

#include <glibmm/i18n.h>
#include <2geom/transforms.h>
#include <gtkmm/adjustment.h>

#include "desktop.h"

#include "display/cairo-utils.h"
#include "display/drawing.h"
#include "display/drawing-context.h"
#include "display/drawing-item.h"
#include "document.h"
#include "document-undo.h"
#include "filter-chemistry.h"
#include "ui/widget/unit-menu.h"
#include "util/units.h"
#include "helper/window.h"
#include "inkscape.h"
#include "ui/interface.h"
#include "macros.h"
#include "message-stack.h"
#include "preferences.h"
#include "selection.h"
#include "sp-filter.h"
#include "sp-namedview.h"
#include "sp-use.h"
#include "style.h"
#include "svg/svg-color.h"
#include "svg/svg.h"
#include "ui/icon-names.h"
#include "ui/widget/spinbutton.h"
#include "unclump.h"
#include "verbs.h"
#include "widgets/icon.h"
#include "xml/repr.h"
#include "sp-root.h"

#include "ui/widget/unit-tracker.h"
#include "widgets/sp-widget.h"
#include "widgets/spw-utilities.h"

#include <string.h>
#include <gtkmm/notebook.h>
#include "ui/widget/notebook-page.h"
#include <gtk/gtk.h>

#include <gtkmm/radiobutton.h>


using Inkscape::DocumentUndo;
using Inkscape::Util::unit_table;

namespace Inkscape {
namespace UI {
namespace Dialog {

#define SB_MARGIN 1
#define VB_MARGIN 4

static Glib::ustring const prefs_path = "/dialogs/dicclonetiler/";

static Inkscape::Drawing *trace_drawing = NULL;
static unsigned trace_visionkey;
static gdouble trace_zoom;
static SPDocument *trace_doc = NULL;


static GtkWidget *lRow;
static GtkWidget *lCol;

static Inkscape::UI::Widget::SpinButton *rowDistanceSpinButton;
static Inkscape::UI::Widget::SpinButton *colDistanceSpinButton;

static Inkscape::UI::Widget::UnitMenu *unit_menu;
static Inkscape::UI::Widget::UnitMenu *unit_col_menu;

static int pattern_shift;
static int pattern_shift_type;
static int pattern_rotational;
static int noOfRepeat;


DicCloneTiler::DicCloneTiler () :
    UI::Widget::Panel ("", "/dialogs/dicclonetiler/", SP_VERB_DIALOG_DIC_CLONETILER),
    dlg(NULL),
    desktop(NULL),
    deskTrack(),
    table_row_labels(NULL)
{
    Gtk::Box *contents = _getContents();
    contents->set_spacing(0);

    pattern_shift = 1;
    pattern_shift_type = 0;
    pattern_rotational = 0;
    noOfRepeat = 1;

    {
        Inkscape::Preferences *prefs = Inkscape::Preferences::get();

        dlg = GTK_WIDGET(gobj());

#if GTK_CHECK_VERSION(3,0,0)
        GtkWidget *mainbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 4);
        gtk_box_set_homogeneous(GTK_BOX(mainbox), FALSE);
#else
        GtkWidget *mainbox = gtk_vbox_new(FALSE, 4);
#endif
        gtk_container_set_border_width (GTK_CONTAINER (mainbox), 6);

        contents->pack_start (*Gtk::manage(Glib::wrap(mainbox)), true, true, 0);

        nb = gtk_notebook_new ();
        gtk_box_pack_start (GTK_BOX (mainbox), nb, FALSE, FALSE, 0);

        table_row_labels = gtk_size_group_new(GTK_SIZE_GROUP_HORIZONTAL);

        // DIC Shift Tab
        {
        	GtkWidget *vb = clonetiler_new_tab (nb, _("DIC S_hift"));
        	GtkWidget *tableShiftButtons = dicclonetiler_table_shift_pattern_buttons (3);
        	gtk_box_pack_start (GTK_BOX (vb), tableShiftButtons, FALSE, FALSE, 0);

            {
            	GtkWidget *l = gtk_label_new ("");
            	gtk_label_set_markup (GTK_LABEL(l), _("Rows:"));
            	gtk_size_group_add_widget(table_row_labels, l);
            	clonetiler_table_attach (tableShiftButtons, l, 0, 4, 1);

            }


            {
            	GtkWidget *l = dicclonetiler_number_of_row_col_spinbox (

            						_("Number of Rows"), "jmax",
									1, 500);
            	clonetiler_table_attach (tableShiftButtons, l, 0, 4, 2);

            }


            {
            	lRow = gtk_label_new ("");
            	gtk_label_set_markup (GTK_LABEL(lRow), _("Row Gap:"));
            	gtk_size_group_add_widget(table_row_labels, lRow);

            	clonetiler_table_attach (tableShiftButtons, lRow, 0, 4, 3);
            }

            {
            	GtkWidget *l = dicclonetiler_row_gap_spinbox();
            	clonetiler_table_attach (tableShiftButtons, l, 0, 4, 4);

            }

            {
            	GtkWidget *l = gtk_label_new ("");
            	gtk_label_set_markup (GTK_LABEL(l), _("Cols:"));
            	gtk_size_group_add_widget(table_row_labels, l);
            	clonetiler_table_attach (tableShiftButtons, l, 0, 5, 1);

            }

            {
            	GtkWidget *l = dicclonetiler_number_of_row_col_spinbox (
            					_("Number of Cols"), "imax",1, 500);
            	clonetiler_table_attach (tableShiftButtons, l, 0, 5, 2);

            }

            {
            	lCol = gtk_label_new ("");
            	gtk_label_set_markup (GTK_LABEL(lCol), _("Col Gap:"));
            	gtk_size_group_add_widget(table_row_labels, lCol);

            	clonetiler_table_attach (tableShiftButtons, lCol, 0, 5, 3);
            }

            {
            	GtkWidget *l = dicclonetiler_col_gap_spinbox();
            	clonetiler_table_attach (tableShiftButtons, l, 0, 5, 4);

            }

        }

        // DIC Rotation Tab
        {
        	GtkWidget *vbr = clonetiler_new_tab (nb, _("DIC _Rotation"));
#if GTK_CHECK_VERSION(3,0,0)
        	GtkWidget *tableRotation = gtk_grid_new();
        	gtk_grid_set_row_spacing(GTK_GRID(tableRotation), 6);
        	gtk_grid_set_column_spacing(GTK_GRID(tableRotation), 0);
#else
        	GtkWidget *tableRotation = gtk_table_new (10, 6, FALSE);
        	gtk_table_set_row_spacings (GTK_TABLE (tableRotation), 6);
        	gtk_table_set_col_spacings (GTK_TABLE (tableRotation), 0);
#endif

        	gtk_container_set_border_width (GTK_CONTAINER(tableRotation), 0);

        	gtk_box_pack_start (GTK_BOX(vbr), tableRotation, FALSE, FALSE, 0);


        	{
        		GtkWidget *l = gtk_label_new ("");

        		gtk_label_set_markup (GTK_LABEL(l), _("Repeats"));

        		gtk_size_group_add_widget(table_row_labels, l);

        		clonetiler_table_attach (tableRotation, l, 0, 5, 2);
        	}

        	/**
        	 * Attaching Spinbutton on the DIC Rotation tab
        	 */

        	{
#if WITH_GTKMM_3_0
        		intAdjustmentRotational = Gtk::Adjustment::create(1.0,1.0,100.0,1.0,2.0,0.0);
#else
        		intAdjustmentRotational = new Gtk::Adjustment (1.0,1.0,100.0,1.0,2.0,0.0);
#endif
        		GtkWidget *spin_repeat = gtk_spin_button_new(intAdjustmentRotational->gobj(),1.0,0);
        		gtk_widget_set_tooltip_text(spin_repeat,"No of Repeats");
        		clonetiler_table_attach(tableRotation, spin_repeat, 0,5,3);
        		g_signal_connect(G_OBJECT(intAdjustmentRotational->gobj()), "value_changed",
        		                           G_CALLBACK(dicclonetiler_number_of_repeat_changed), NULL);
        	}
        }

        {
#if GTK_CHECK_VERSION(3,0,0)
            GtkWidget *hb = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, VB_MARGIN);
            gtk_box_set_homogeneous(GTK_BOX(hb), FALSE);
#else
            GtkWidget *hb = gtk_hbox_new(FALSE, VB_MARGIN);
#endif
            gtk_box_pack_start (GTK_BOX (mainbox), hb, FALSE, FALSE, 0);
            GtkWidget *l = gtk_label_new("");
            gtk_label_set_markup (GTK_LABEL(l), _("Apply to tiled clones:"));
            gtk_box_pack_start (GTK_BOX (hb), l, FALSE, FALSE, 0);
        }

        // Statusbar
        {
#if GTK_CHECK_VERSION(3,0,0)
            GtkWidget *hb = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, VB_MARGIN);
            gtk_box_set_homogeneous(GTK_BOX(hb), FALSE);
#else
            GtkWidget *hb = gtk_hbox_new(FALSE, VB_MARGIN);
#endif
            gtk_box_pack_end (GTK_BOX (mainbox), hb, FALSE, FALSE, 0);
            GtkWidget *l = gtk_label_new("");
            g_object_set_data (G_OBJECT(dlg), "status", (gpointer) l);
            gtk_box_pack_start (GTK_BOX (hb), l, FALSE, FALSE, 0);
        }

        // Buttons
        {
#if GTK_CHECK_VERSION(3,0,0)
            GtkWidget *hb = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, VB_MARGIN);
            gtk_box_set_homogeneous(GTK_BOX(hb), FALSE);
#else
            GtkWidget *hb = gtk_hbox_new(FALSE, VB_MARGIN);
#endif
            gtk_box_pack_start (GTK_BOX (mainbox), hb, FALSE, FALSE, 0);

            {
                GtkWidget *b = gtk_button_new ();
                GtkWidget *l = gtk_label_new ("");
                gtk_label_set_markup_with_mnemonic (GTK_LABEL(l), _(" <b>_Create</b> "));
                gtk_container_add (GTK_CONTAINER(b), l);
                gtk_widget_set_tooltip_text (b, _("Create and tile the clones of the selection"));
                g_signal_connect (G_OBJECT (b), "clicked", G_CALLBACK (dicclonetiler_apply), dlg);
                gtk_box_pack_end (GTK_BOX (hb), b, FALSE, FALSE, 0);
            }

            { // buttons which are enabled only when there are tiled clones
#if GTK_CHECK_VERSION(3,0,0)
                GtkWidget *sb = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
                gtk_box_set_homogeneous(GTK_BOX(sb), FALSE);
#else
                GtkWidget *sb = gtk_hbox_new(FALSE, 0);
#endif
                gtk_box_pack_end (GTK_BOX (hb), sb, FALSE, FALSE, 0);
                g_object_set_data (G_OBJECT(dlg), "buttons_on_tiles", (gpointer) sb);
                {
                    // TRANSLATORS: if a group of objects are "clumped" together, then they
                    //  are unevenly spread in the given amount of space - as shown in the
                    //  diagrams on the left in the following screenshot:
                    //  http://www.inkscape.org/screenshots/gallery/inkscape-0.42-CVS-tiles-unclump.png
                    //  So unclumping is the process of spreading a number of objects out more evenly.
                    ///GtkWidget *b = gtk_button_new_with_mnemonic (_(" _Unclump "));
                    ///gtk_widget_set_tooltip_text (b, _("Spread out clones to reduce clumping; can be applied repeatedly"));
                    ///g_signal_connect (G_OBJECT (b), "clicked", G_CALLBACK (clonetiler_unclump), NULL);
                    ///gtk_box_pack_end (GTK_BOX (sb), b, FALSE, FALSE, 0);
                }

                {
                    GtkWidget *b = gtk_button_new_with_mnemonic (_(" Re_move "));
                    gtk_widget_set_tooltip_text (b, _("Remove existing tiled clones of the selected object (siblings only)"));
                    g_signal_connect (G_OBJECT (b), "clicked", G_CALLBACK (clonetiler_remove), gpointer(dlg));
                    gtk_box_pack_end (GTK_BOX (sb), b, FALSE, FALSE, 0);
                }

                // connect to global selection changed signal (so we can change desktops) and
                // external_change (so we're not fooled by undo)
                selectChangedConn = INKSCAPE.signal_selection_changed.connect(sigc::bind(sigc::ptr_fun(&DicCloneTiler::clonetiler_change_selection), dlg));
                externChangedConn = INKSCAPE.signal_external_change.connect   (sigc::bind(sigc::ptr_fun(&DicCloneTiler::clonetiler_external_change), dlg));

                g_signal_connect(G_OBJECT(dlg), "destroy", G_CALLBACK(clonetiler_disconnect_gsignal), this);

                // update now
                clonetiler_change_selection (SP_ACTIVE_DESKTOP->getSelection(), dlg);
            }

            Glib::wrap(GTK_NOTEBOOK(nb))->signal_switch_page().connect(sigc::mem_fun(this,&DicCloneTiler::_ondicSwitchPage));
        }

        gtk_widget_show_all (mainbox);
    }

    show_all();

    desktopChangeConn = deskTrack.connectDesktopChanged( sigc::mem_fun(*this, &DicCloneTiler::setTargetDesktop) );
    deskTrack.connect(GTK_WIDGET(gobj()));

}

DicCloneTiler::~DicCloneTiler (void)
{
    //subselChangedConn.disconnect();
    //selectChangedConn.disconnect();
    //selectModifiedConn.disconnect();
    desktopChangeConn.disconnect();
    deskTrack.disconnect();
    color_changed_connection.disconnect();
}

void DicCloneTiler::setDesktop(SPDesktop *desktop)
{
    Panel::setDesktop(desktop);
    deskTrack.setBase(desktop);
}

void DicCloneTiler::setTargetDesktop(SPDesktop *desktop)
{
    if (this->desktop != desktop) {
        if (this->desktop) {
            //selectModifiedConn.disconnect();
            //subselChangedConn.disconnect();
            //selectChangedConn.disconnect();
        }
        this->desktop = desktop;
        if (desktop && desktop->selection) {
            //selectChangedConn = desktop->selection->connectChanged(sigc::hide(sigc::mem_fun(*this, &DicCloneTiler::clonetiler_change_selection)));
            //subselChangedConn = desktop->connectToolSubselectionChanged(sigc::hide(sigc::mem_fun(*this, &DicCloneTiler::clonetiler_change_selection)));
            //selectModifiedConn = desktop->selection->connectModified(sigc::hide<0>(sigc::mem_fun(*this, &DicCloneTiler::clonetiler_change_selection)));
        }
    }
}

void DicCloneTiler::on_picker_color_changed(guint rgba)
{
    static bool is_updating = false;
    if (is_updating || !SP_ACTIVE_DESKTOP)
        return;

    is_updating = true;

    gchar c[32];
    sp_svg_write_color(c, sizeof(c), rgba);
    Inkscape::Preferences *prefs = Inkscape::Preferences::get();
    prefs->setString(prefs_path + "initial_color", c);

    is_updating = false;
}

void DicCloneTiler::clonetiler_change_selection(Inkscape::Selection *selection, GtkWidget *dlg)
{
    GtkWidget *buttons = GTK_WIDGET(g_object_get_data (G_OBJECT(dlg), "buttons_on_tiles"));
    GtkWidget *status = GTK_WIDGET(g_object_get_data (G_OBJECT(dlg), "status"));

    if (selection->isEmpty()) {
        gtk_widget_set_sensitive (buttons, FALSE);
        gtk_label_set_markup (GTK_LABEL(status), _("<small>Nothing selected.</small>"));
        return;
    }

    if (selection->itemList().size() > 1) {
        gtk_widget_set_sensitive (buttons, FALSE);
        gtk_label_set_markup (GTK_LABEL(status), _("<small>More than one object selected.</small>"));
        return;
    }

    guint n = clonetiler_number_of_clones(selection->singleItem());
    if (n > 0) {
        gtk_widget_set_sensitive (buttons, TRUE);
        gchar *sta = g_strdup_printf (_("<small>Object has <b>%d</b> tiled clones.</small>"), n);
        gtk_label_set_markup (GTK_LABEL(status), sta);
        g_free (sta);
    } else {
        gtk_widget_set_sensitive (buttons, FALSE);
        gtk_label_set_markup (GTK_LABEL(status), _("<small>Object has no tiled clones.</small>"));
    }
}

void DicCloneTiler::clonetiler_external_change(GtkWidget *dlg)
{
    clonetiler_change_selection (SP_ACTIVE_DESKTOP->getSelection(), dlg);
}

void DicCloneTiler::clonetiler_disconnect_gsignal(GObject *, gpointer source)
{
    g_return_if_fail(source != NULL);

    DicCloneTiler* dlg = reinterpret_cast<DicCloneTiler*>(source);
    dlg->selectChangedConn.disconnect();
    dlg->externChangedConn.disconnect();
}

Geom::Affine DicCloneTiler::clonetiler_get_transform(
    // symmetry group
    int type,

    // row, column
    int i, int j,

    // center, width, height of the tile
    double cx, double cy,
    double w,  double h,

    // values from the dialog:
    // Shift
    double shiftx_per_i,      double shifty_per_i,
    double shiftx_per_j,      double shifty_per_j,
    double shiftx_rand,       double shifty_rand,
    double shiftx_exp,        double shifty_exp,
    int    shiftx_alternate,  int    shifty_alternate,
    int    shiftx_cumulate,   int    shifty_cumulate,
    int    shiftx_excludew,   int    shifty_excludeh,

    // Scale
    double scalex_per_i,      double scaley_per_i,
    double scalex_per_j,      double scaley_per_j,
    double scalex_rand,       double scaley_rand,
    double scalex_exp,        double scaley_exp,
    double scalex_log,        double scaley_log,
    int    scalex_alternate,  int    scaley_alternate,
    int    scalex_cumulate,   int    scaley_cumulate,

    // Rotation
    double rotate_per_i,      double rotate_per_j,
    double rotate_rand,
    int    rotate_alternatei, int    rotate_alternatej,
    int    rotate_cumulatei,  int    rotate_cumulatej
    )
{

    // Shift (in units of tile width or height) -------------
    double delta_shifti = 0.0;
    double delta_shiftj = 0.0;

    if( shiftx_alternate ) {
        delta_shifti = (double)(i%2);
    } else {
        if( shiftx_cumulate ) {  // Should the delta shifts be cumulative (i.e. 1, 1+2, 1+2+3, ...)
            delta_shifti = (double)(i*i);
        } else {
            delta_shifti = (double)i;
        }
    }

    if( shifty_alternate ) {
        delta_shiftj = (double)(j%2);
    } else {
        if( shifty_cumulate ) {
            delta_shiftj = (double)(j*j);
        } else {
            delta_shiftj = (double)j;
        }
    }

    // Random shift, only calculate if non-zero.
    double delta_shiftx_rand = 0.0;
    double delta_shifty_rand = 0.0;
    if( shiftx_rand != 0.0 ) delta_shiftx_rand = shiftx_rand * g_random_double_range (-1, 1);
    if( shifty_rand != 0.0 ) delta_shifty_rand = shifty_rand * g_random_double_range (-1, 1);


    // Delta shift (units of tile width/height)
    double di = shiftx_per_i * delta_shifti  + shiftx_per_j * delta_shiftj + delta_shiftx_rand;
    double dj = shifty_per_i * delta_shifti  + shifty_per_j * delta_shiftj + delta_shifty_rand;

    // Shift in actual x and y, used below
    double dx = w * di;
    double dy = h * dj;

    double shifti = di;
    double shiftj = dj;

    // Include tile width and height in shift if required
    if( !shiftx_excludew ) shifti += i;
    if( !shifty_excludeh ) shiftj += j;

    // Add exponential shift if necessary
    if ( shiftx_exp != 1.0 ) shifti = pow( shifti, shiftx_exp );
    if ( shifty_exp != 1.0 ) shiftj = pow( shiftj, shifty_exp );

    // Final shift
    Geom::Affine rect_translate (Geom::Translate (w * shifti, h * shiftj));

    // Rotation (in degrees) ------------
    double delta_rotationi = 0.0;
    double delta_rotationj = 0.0;

    if( rotate_alternatei ) {
        delta_rotationi = (double)(i%2);
    } else {
        if( rotate_cumulatei ) {
            delta_rotationi = (double)(i*i + i)/2.0;
        } else {
            delta_rotationi = (double)i;
        }
    }

    if( rotate_alternatej ) {
        delta_rotationj = (double)(j%2);
    } else {
        if( rotate_cumulatej ) {
            delta_rotationj = (double)(j*j + j)/2.0;
        } else {
            delta_rotationj = (double)j;
        }
    }

    double delta_rotate_rand = 0.0;
    if( rotate_rand != 0.0 ) delta_rotate_rand = rotate_rand * 180.0 * g_random_double_range (-1, 1);

    double dr = rotate_per_i * delta_rotationi + rotate_per_j * delta_rotationj + delta_rotate_rand;

    // Scale (times the original) -----------
    double delta_scalei = 0.0;
    double delta_scalej = 0.0;

    if( scalex_alternate ) {
        delta_scalei = (double)(i%2);
    } else {
        if( scalex_cumulate ) {  // Should the delta scales be cumulative (i.e. 1, 1+2, 1+2+3, ...)
            delta_scalei = (double)(i*i + i)/2.0;
        } else {
            delta_scalei = (double)i;
        }
    }

    if( scaley_alternate ) {
        delta_scalej = (double)(j%2);
    } else {
        if( scaley_cumulate ) {
            delta_scalej = (double)(j*j + j)/2.0;
        } else {
            delta_scalej = (double)j;
        }
    }

    // Random scale, only calculate if non-zero.
    double delta_scalex_rand = 0.0;
    double delta_scaley_rand = 0.0;
    if( scalex_rand != 0.0 ) delta_scalex_rand = scalex_rand * g_random_double_range (-1, 1);
    if( scaley_rand != 0.0 ) delta_scaley_rand = scaley_rand * g_random_double_range (-1, 1);
    // But if random factors are same, scale x and y proportionally
    if( scalex_rand == scaley_rand ) delta_scalex_rand = delta_scaley_rand;

    // Total delta scale
    double scalex = 1.0 + scalex_per_i * delta_scalei  + scalex_per_j * delta_scalej + delta_scalex_rand;
    double scaley = 1.0 + scaley_per_i * delta_scalei  + scaley_per_j * delta_scalej + delta_scaley_rand;

    if( scalex < 0.0 ) scalex = 0.0;
    if( scaley < 0.0 ) scaley = 0.0;

    // Add exponential scale if necessary
    if ( scalex_exp != 1.0 ) scalex = pow( scalex, scalex_exp );
    if ( scaley_exp != 1.0 ) scaley = pow( scaley, scaley_exp );

    // Add logarithmic factor if necessary
    if ( scalex_log  > 0.0 ) scalex = pow( scalex_log, scalex - 1.0 );
    if ( scaley_log  > 0.0 ) scaley = pow( scaley_log, scaley - 1.0 );
    // Alternative using rotation angle
    //if ( scalex_log  != 1.0 ) scalex *= pow( scalex_log, M_PI*dr/180 );
    //if ( scaley_log  != 1.0 ) scaley *= pow( scaley_log, M_PI*dr/180 );


    // Calculate transformation matrices, translating back to "center of tile" (rotation center) before transforming
    Geom::Affine drot_c   = Geom::Translate(-cx, -cy) * Geom::Rotate (M_PI*dr/180)    * Geom::Translate(cx, cy);

    Geom::Affine dscale_c = Geom::Translate(-cx, -cy) * Geom::Scale (scalex, scaley)  * Geom::Translate(cx, cy);

    Geom::Affine d_s_r = dscale_c * drot_c;

    Geom::Affine rotate_180_c  = Geom::Translate(-cx, -cy) * Geom::Rotate (M_PI)      * Geom::Translate(cx, cy);

    Geom::Affine rotate_90_c   = Geom::Translate(-cx, -cy) * Geom::Rotate (-M_PI/2)   * Geom::Translate(cx, cy);
    Geom::Affine rotate_m90_c  = Geom::Translate(-cx, -cy) * Geom::Rotate ( M_PI/2)   * Geom::Translate(cx, cy);

    Geom::Affine rotate_120_c  = Geom::Translate(-cx, -cy) * Geom::Rotate (-2*M_PI/3) * Geom::Translate(cx, cy);
    Geom::Affine rotate_m120_c = Geom::Translate(-cx, -cy) * Geom::Rotate ( 2*M_PI/3) * Geom::Translate(cx, cy);

    Geom::Affine rotate_60_c   = Geom::Translate(-cx, -cy) * Geom::Rotate (-M_PI/3)   * Geom::Translate(cx, cy);
    Geom::Affine rotate_m60_c  = Geom::Translate(-cx, -cy) * Geom::Rotate ( M_PI/3)   * Geom::Translate(cx, cy);

    Geom::Affine flip_x        = Geom::Translate(-cx, -cy) * Geom::Scale (-1, 1)      * Geom::Translate(cx, cy);
    Geom::Affine flip_y        = Geom::Translate(-cx, -cy) * Geom::Scale (1, -1)      * Geom::Translate(cx, cy);


    // Create tile with required symmetry
    const double cos60 = cos(M_PI/3);
    const double sin60 = sin(M_PI/3);
    const double cos30 = cos(M_PI/6);
    const double sin30 = sin(M_PI/6);

    switch (type) {

    case DIC_TILE_P1:
        return d_s_r * rect_translate;
        break;

    case DIC_TILE_P2:
        if (i % 2 == 0) {
            return d_s_r * rect_translate;
        } else {
            return d_s_r * rotate_180_c * rect_translate;
        }
        break;

    case DIC_TILE_PM:
        if (i % 2 == 0) {
            return d_s_r * rect_translate;
        } else {
            return d_s_r * flip_x * rect_translate;
        }
        break;

    case DIC_TILE_PG:
        if (j % 2 == 0) {
            return d_s_r * rect_translate;
        } else {
            return d_s_r * flip_x * rect_translate;
        }
        break;

    case DIC_TILE_CM:
        if ((i + j) % 2 == 0) {
            return d_s_r * rect_translate;
        } else {
            return d_s_r * flip_x * rect_translate;
        }
        break;

    case DIC_TILE_PMM:
        if (j % 2 == 0) {
            if (i % 2 == 0) {
                return d_s_r * rect_translate;
            } else {
                return d_s_r * flip_x * rect_translate;
            }
        } else {
            if (i % 2 == 0) {
                return d_s_r * flip_y * rect_translate;
            } else {
                return d_s_r * flip_x * flip_y * rect_translate;
            }
        }
        break;

    case DIC_TILE_PMG:
        if (j % 2 == 0) {
            if (i % 2 == 0) {
                return d_s_r * rect_translate;
            } else {
                return d_s_r * rotate_180_c * rect_translate;
            }
        } else {
            if (i % 2 == 0) {
                return d_s_r * flip_y * rect_translate;
            } else {
                return d_s_r * rotate_180_c * flip_y * rect_translate;
            }
        }
        break;

    case DIC_TILE_PGG:
        if (j % 2 == 0) {
            if (i % 2 == 0) {
                return d_s_r * rect_translate;
            } else {
                return d_s_r * flip_y * rect_translate;
            }
        } else {
            if (i % 2 == 0) {
                return d_s_r * rotate_180_c * rect_translate;
            } else {
                return d_s_r * rotate_180_c * flip_y * rect_translate;
            }
        }
        break;

    case DIC_TILE_CMM:
        if (j % 4 == 0) {
            if (i % 2 == 0) {
                return d_s_r * rect_translate;
            } else {
                return d_s_r * flip_x * rect_translate;
            }
        } else if (j % 4 == 1) {
            if (i % 2 == 0) {
                return d_s_r * flip_y * rect_translate;
            } else {
                return d_s_r * flip_x * flip_y * rect_translate;
            }
        } else if (j % 4 == 2) {
            if (i % 2 == 1) {
                return d_s_r * rect_translate;
            } else {
                return d_s_r * flip_x * rect_translate;
            }
        } else {
            if (i % 2 == 1) {
                return d_s_r * flip_y * rect_translate;
            } else {
                return d_s_r * flip_x * flip_y * rect_translate;
            }
        }
        break;

    case DIC_TILE_P4:
    {
        Geom::Affine ori  (Geom::Translate ((w + h) * pow((i/2), shiftx_exp) + dx,  (h + w) * pow((j/2), shifty_exp) + dy));
        Geom::Affine dia1 (Geom::Translate (w/2 + h/2, -h/2 + w/2));
        Geom::Affine dia2 (Geom::Translate (-w/2 + h/2, h/2 + w/2));
        if (j % 2 == 0) {
            if (i % 2 == 0) {
                return d_s_r * ori;
            } else {
                return d_s_r * rotate_m90_c * dia1 * ori;
            }
        } else {
            if (i % 2 == 0) {
                return d_s_r * rotate_90_c * dia2 * ori;
            } else {
                return d_s_r * rotate_180_c * dia1 * dia2 * ori;
            }
        }
    }
    break;

    case DIC_TILE_P4M:
    {
        double max = MAX(w, h);
        Geom::Affine ori (Geom::Translate ((max + max) * pow((i/4), shiftx_exp) + dx,  (max + max) * pow((j/2), shifty_exp) + dy));
        Geom::Affine dia1 (Geom::Translate ( w/2 - h/2, h/2 - w/2));
        Geom::Affine dia2 (Geom::Translate (-h/2 + w/2, w/2 - h/2));
        if (j % 2 == 0) {
            if (i % 4 == 0) {
                return d_s_r * ori;
            } else if (i % 4 == 1) {
                return d_s_r * flip_y * rotate_m90_c * dia1 * ori;
            } else if (i % 4 == 2) {
                return d_s_r * rotate_m90_c * dia1 * Geom::Translate (h, 0) * ori;
            } else if (i % 4 == 3) {
                return d_s_r * flip_x * Geom::Translate (w, 0) * ori;
            }
        } else {
            if (i % 4 == 0) {
                return d_s_r * flip_y * Geom::Translate(0, h) * ori;
            } else if (i % 4 == 1) {
                return d_s_r * rotate_90_c * dia2 * Geom::Translate(0, h) * ori;
            } else if (i % 4 == 2) {
                return d_s_r * flip_y * rotate_90_c * dia2 * Geom::Translate(h, 0) * Geom::Translate(0, h) * ori;
            } else if (i % 4 == 3) {
                return d_s_r * flip_y * flip_x * Geom::Translate(w, 0) * Geom::Translate(0, h) * ori;
            }
        }
    }
    break;

    case DIC_TILE_P4G:
    {
        double max = MAX(w, h);
        Geom::Affine ori (Geom::Translate ((max + max) * pow((i/4), shiftx_exp) + dx,  (max + max) * pow(j, shifty_exp) + dy));
        Geom::Affine dia1 (Geom::Translate ( w/2 + h/2, h/2 - w/2));
        Geom::Affine dia2 (Geom::Translate (-h/2 + w/2, w/2 + h/2));
        if (((i/4) + j) % 2 == 0) {
            if (i % 4 == 0) {
                return d_s_r * ori;
            } else if (i % 4 == 1) {
                return d_s_r * rotate_m90_c * dia1 * ori;
            } else if (i % 4 == 2) {
                return d_s_r * rotate_90_c * dia2 * ori;
            } else if (i % 4 == 3) {
                return d_s_r * rotate_180_c * dia1 * dia2 * ori;
            }
        } else {
            if (i % 4 == 0) {
                return d_s_r * flip_y * Geom::Translate (0, h) * ori;
            } else if (i % 4 == 1) {
                return d_s_r * flip_y * rotate_m90_c * dia1 * Geom::Translate (-h, 0) * ori;
            } else if (i % 4 == 2) {
                return d_s_r * flip_y * rotate_90_c * dia2 * Geom::Translate (h, 0) * ori;
            } else if (i % 4 == 3) {
                return d_s_r * flip_x * Geom::Translate (w, 0) * ori;
            }
        }
    }
    break;

    case DIC_TILE_P3:
    {
        double width;
        double height;
        Geom::Affine dia1;
        Geom::Affine dia2;
        if (w > h) {
            width  = w + w * cos60;
            height = 2 * w * sin60;
            dia1 = Geom::Affine (Geom::Translate (w/2 + w/2 * cos60, -(w/2 * sin60)));
            dia2 = dia1 * Geom::Affine (Geom::Translate (0, 2 * (w/2 * sin60)));
        } else {
            width = h * cos (M_PI/6);
            height = h;
            dia1 = Geom::Affine (Geom::Translate (h/2 * cos30, -(h/2 * sin30)));
            dia2 = dia1 * Geom::Affine (Geom::Translate (0, h/2));
        }
        Geom::Affine ori (Geom::Translate (width * pow((2*(i/3) + j%2), shiftx_exp) + dx,  (height/2) * pow(j, shifty_exp) + dy));
        if (i % 3 == 0) {
            return d_s_r * ori;
        } else if (i % 3 == 1) {
            return d_s_r * rotate_m120_c * dia1 * ori;
        } else if (i % 3 == 2) {
            return d_s_r * rotate_120_c * dia2 * ori;
        }
    }
    break;

    case DIC_TILE_P31M:
    {
        Geom::Affine ori;
        Geom::Affine dia1;
        Geom::Affine dia2;
        Geom::Affine dia3;
        Geom::Affine dia4;
        if (w > h) {
            ori = Geom::Affine(Geom::Translate (w * pow((i/6) + 0.5*(j%2), shiftx_exp) + dx,  (w * cos30) * pow(j, shifty_exp) + dy));
            dia1 = Geom::Affine (Geom::Translate (0, h/2) * Geom::Translate (w/2, 0) * Geom::Translate (w/2 * cos60, -w/2 * sin60) * Geom::Translate (-h/2 * cos30, -h/2 * sin30) );
            dia2 = dia1 * Geom::Affine (Geom::Translate (h * cos30, h * sin30));
            dia3 = dia2 * Geom::Affine (Geom::Translate (0, 2 * (w/2 * sin60 - h/2 * sin30)));
            dia4 = dia3 * Geom::Affine (Geom::Translate (-h * cos30, h * sin30));
        } else {
            ori  = Geom::Affine (Geom::Translate (2*h * cos30  * pow((i/6 + 0.5*(j%2)), shiftx_exp) + dx,  (2*h - h * sin30) * pow(j, shifty_exp) + dy));
            dia1 = Geom::Affine (Geom::Translate (0, -h/2) * Geom::Translate (h/2 * cos30, h/2 * sin30));
            dia2 = dia1 * Geom::Affine (Geom::Translate (h * cos30, h * sin30));
            dia3 = dia2 * Geom::Affine (Geom::Translate (0, h/2));
            dia4 = dia3 * Geom::Affine (Geom::Translate (-h * cos30, h * sin30));
        }
        if (i % 6 == 0) {
            return d_s_r * ori;
        } else if (i % 6 == 1) {
            return d_s_r * flip_y * rotate_m120_c * dia1 * ori;
        } else if (i % 6 == 2) {
            return d_s_r * rotate_m120_c * dia2 * ori;
        } else if (i % 6 == 3) {
            return d_s_r * flip_y * rotate_120_c * dia3 * ori;
        } else if (i % 6 == 4) {
            return d_s_r * rotate_120_c * dia4 * ori;
        } else if (i % 6 == 5) {
            return d_s_r * flip_y * Geom::Translate(0, h) * ori;
        }
    }
    break;

    case DIC_TILE_P3M1:
    {
        double width;
        double height;
        Geom::Affine dia1;
        Geom::Affine dia2;
        Geom::Affine dia3;
        Geom::Affine dia4;
        if (w > h) {
            width = w + w * cos60;
            height = 2 * w * sin60;
            dia1 = Geom::Affine (Geom::Translate (0, h/2) * Geom::Translate (w/2, 0) * Geom::Translate (w/2 * cos60, -w/2 * sin60) * Geom::Translate (-h/2 * cos30, -h/2 * sin30) );
            dia2 = dia1 * Geom::Affine (Geom::Translate (h * cos30, h * sin30));
            dia3 = dia2 * Geom::Affine (Geom::Translate (0, 2 * (w/2 * sin60 - h/2 * sin30)));
            dia4 = dia3 * Geom::Affine (Geom::Translate (-h * cos30, h * sin30));
        } else {
            width = 2 * h * cos (M_PI/6);
            height = 2 * h;
            dia1 = Geom::Affine (Geom::Translate (0, -h/2) * Geom::Translate (h/2 * cos30, h/2 * sin30));
            dia2 = dia1 * Geom::Affine (Geom::Translate (h * cos30, h * sin30));
            dia3 = dia2 * Geom::Affine (Geom::Translate (0, h/2));
            dia4 = dia3 * Geom::Affine (Geom::Translate (-h * cos30, h * sin30));
        }
        Geom::Affine ori (Geom::Translate (width * pow((2*(i/6) + j%2), shiftx_exp) + dx,  (height/2) * pow(j, shifty_exp) + dy));
        if (i % 6 == 0) {
            return d_s_r * ori;
        } else if (i % 6 == 1) {
            return d_s_r * flip_y * rotate_m120_c * dia1 * ori;
        } else if (i % 6 == 2) {
            return d_s_r * rotate_m120_c * dia2 * ori;
        } else if (i % 6 == 3) {
            return d_s_r * flip_y * rotate_120_c * dia3 * ori;
        } else if (i % 6 == 4) {
            return d_s_r * rotate_120_c * dia4 * ori;
        } else if (i % 6 == 5) {
            return d_s_r * flip_y * Geom::Translate(0, h) * ori;
        }
    }
    break;

    case DIC_TILE_P6:
    {
        Geom::Affine ori;
        Geom::Affine dia1;
        Geom::Affine dia2;
        Geom::Affine dia3;
        Geom::Affine dia4;
        Geom::Affine dia5;
        if (w > h) {
            ori = Geom::Affine(Geom::Translate (w * pow((2*(i/6) + (j%2)), shiftx_exp) + dx,  (2*w * sin60) * pow(j, shifty_exp) + dy));
            dia1 = Geom::Affine (Geom::Translate (w/2 * cos60, -w/2 * sin60));
            dia2 = dia1 * Geom::Affine (Geom::Translate (w/2, 0));
            dia3 = dia2 * Geom::Affine (Geom::Translate (w/2 * cos60, w/2 * sin60));
            dia4 = dia3 * Geom::Affine (Geom::Translate (-w/2 * cos60, w/2 * sin60));
            dia5 = dia4 * Geom::Affine (Geom::Translate (-w/2, 0));
        } else {
            ori = Geom::Affine(Geom::Translate (2*h * cos30 * pow((i/6 + 0.5*(j%2)), shiftx_exp) + dx,  (h + h * sin30) * pow(j, shifty_exp) + dy));
            dia1 = Geom::Affine (Geom::Translate (-w/2, -h/2) * Geom::Translate (h/2 * cos30, -h/2 * sin30) * Geom::Translate (w/2 * cos60, w/2 * sin60));
            dia2 = dia1 * Geom::Affine (Geom::Translate (-w/2 * cos60, -w/2 * sin60) * Geom::Translate (h/2 * cos30, -h/2 * sin30) * Geom::Translate (h/2 * cos30, h/2 * sin30) * Geom::Translate (-w/2 * cos60, w/2 * sin60));
            dia3 = dia2 * Geom::Affine (Geom::Translate (w/2 * cos60, -w/2 * sin60) * Geom::Translate (h/2 * cos30, h/2 * sin30) * Geom::Translate (-w/2, h/2));
            dia4 = dia3 * dia1.inverse();
            dia5 = dia3 * dia2.inverse();
        }
        if (i % 6 == 0) {
            return d_s_r * ori;
        } else if (i % 6 == 1) {
            return d_s_r * rotate_m60_c * dia1 * ori;
        } else if (i % 6 == 2) {
            return d_s_r * rotate_m120_c * dia2 * ori;
        } else if (i % 6 == 3) {
            return d_s_r * rotate_180_c * dia3 * ori;
        } else if (i % 6 == 4) {
            return d_s_r * rotate_120_c * dia4 * ori;
        } else if (i % 6 == 5) {
            return d_s_r * rotate_60_c * dia5 * ori;
        }
    }
    break;

    case DIC_TILE_P6M:
    {

        Geom::Affine ori;
        Geom::Affine dia1, dia2, dia3, dia4, dia5, dia6, dia7, dia8, dia9, dia10;
        if (w > h) {
            ori = Geom::Affine(Geom::Translate (w * pow((2*(i/12) + (j%2)), shiftx_exp) + dx,  (2*w * sin60) * pow(j, shifty_exp) + dy));
            dia1 = Geom::Affine (Geom::Translate (w/2, h/2) * Geom::Translate (-w/2 * cos60, -w/2 * sin60) * Geom::Translate (-h/2 * cos30, h/2 * sin30));
            dia2 = dia1 * Geom::Affine (Geom::Translate (h * cos30, -h * sin30));
            dia3 = dia2 * Geom::Affine (Geom::Translate (-h/2 * cos30, h/2 * sin30) * Geom::Translate (w * cos60, 0) * Geom::Translate (-h/2 * cos30, -h/2 * sin30));
            dia4 = dia3 * Geom::Affine (Geom::Translate (h * cos30, h * sin30));
            dia5 = dia4 * Geom::Affine (Geom::Translate (-h/2 * cos30, -h/2 * sin30) * Geom::Translate (-w/2 * cos60, w/2 * sin60) * Geom::Translate (w/2, -h/2));
            dia6 = dia5 * Geom::Affine (Geom::Translate (0, h));
            dia7 = dia6 * dia1.inverse();
            dia8 = dia6 * dia2.inverse();
            dia9 = dia6 * dia3.inverse();
            dia10 = dia6 * dia4.inverse();
        } else {
            ori = Geom::Affine(Geom::Translate (4*h * cos30 * pow((i/12 + 0.5*(j%2)), shiftx_exp) + dx,  (2*h  + 2*h * sin30) * pow(j, shifty_exp) + dy));
            dia1 = Geom::Affine (Geom::Translate (-w/2, -h/2) * Geom::Translate (h/2 * cos30, -h/2 * sin30) * Geom::Translate (w/2 * cos60, w/2 * sin60));
            dia2 = dia1 * Geom::Affine (Geom::Translate (h * cos30, -h * sin30));
            dia3 = dia2 * Geom::Affine (Geom::Translate (-w/2 * cos60, -w/2 * sin60) * Geom::Translate (h * cos30, 0) * Geom::Translate (-w/2 * cos60, w/2 * sin60));
            dia4 = dia3 * Geom::Affine (Geom::Translate (h * cos30, h * sin30));
            dia5 = dia4 * Geom::Affine (Geom::Translate (w/2 * cos60, -w/2 * sin60) * Geom::Translate (h/2 * cos30, h/2 * sin30) * Geom::Translate (-w/2, h/2));
            dia6 = dia5 * Geom::Affine (Geom::Translate (0, h));
            dia7 = dia6 * dia1.inverse();
            dia8 = dia6 * dia2.inverse();
            dia9 = dia6 * dia3.inverse();
            dia10 = dia6 * dia4.inverse();
        }
        if (i % 12 == 0) {
            return d_s_r * ori;
        } else if (i % 12 == 1) {
            return d_s_r * flip_y * rotate_m60_c * dia1 * ori;
        } else if (i % 12 == 2) {
            return d_s_r * rotate_m60_c * dia2 * ori;
        } else if (i % 12 == 3) {
            return d_s_r * flip_y * rotate_m120_c * dia3 * ori;
        } else if (i % 12 == 4) {
            return d_s_r * rotate_m120_c * dia4 * ori;
        } else if (i % 12 == 5) {
            return d_s_r * flip_x * dia5 * ori;
        } else if (i % 12 == 6) {
            return d_s_r * flip_x * flip_y * dia6 * ori;
        } else if (i % 12 == 7) {
            return d_s_r * flip_y * rotate_120_c * dia7 * ori;
        } else if (i % 12 == 8) {
            return d_s_r * rotate_120_c * dia8 * ori;
        } else if (i % 12 == 9) {
            return d_s_r * flip_y * rotate_60_c * dia9 * ori;
        } else if (i % 12 == 10) {
            return d_s_r * rotate_60_c * dia10 * ori;
        } else if (i % 12 == 11) {
            return d_s_r * flip_y * Geom::Translate (0, h) * ori;
        }
    }
    break;

    default:
        break;
    }

    return Geom::identity();
}

bool DicCloneTiler::clonetiler_is_a_clone_of(SPObject *tile, SPObject *obj)
{
    bool result = false;
    char *id_href = NULL;

    if (obj) {
        Inkscape::XML::Node *obj_repr = obj->getRepr();
        id_href = g_strdup_printf("#%s", obj_repr->attribute("id"));
    }

    if (dynamic_cast<SPUse *>(tile) &&
        tile->getRepr()->attribute("xlink:href") &&
        (!id_href || !strcmp(id_href, tile->getRepr()->attribute("xlink:href"))) &&
        tile->getRepr()->attribute("inkscape:tiled-clone-of") &&
        (!id_href || !strcmp(id_href, tile->getRepr()->attribute("inkscape:tiled-clone-of"))))
    {
        result = true;
    } else {
        result = false;
    }
    if (id_href) {
        g_free(id_href);
        id_href = 0;
    }
    return result;
}

void DicCloneTiler::clonetiler_trace_hide_tiled_clones_recursively(SPObject *from)
{
    if (!trace_drawing)
        return;

    for (SPObject *o = from->firstChild(); o != NULL; o = o->next) {
        SPItem *item = dynamic_cast<SPItem *>(o);
        if (item && clonetiler_is_a_clone_of(o, NULL)) {
            item->invoke_hide(trace_visionkey); // FIXME: hide each tiled clone's original too!
        }
        clonetiler_trace_hide_tiled_clones_recursively (o);
    }
}

void DicCloneTiler::clonetiler_trace_setup(SPDocument *doc, gdouble zoom, SPItem *original)
{
    trace_drawing = new Inkscape::Drawing();
    /* Create ArenaItem and set transform */
    trace_visionkey = SPItem::display_key_new(1);
    trace_doc = doc;
    trace_drawing->setRoot(trace_doc->getRoot()->invoke_show(*trace_drawing, trace_visionkey, SP_ITEM_SHOW_DISPLAY));

    // hide the (current) original and any tiled clones, we only want to pick the background
    original->invoke_hide(trace_visionkey);
    clonetiler_trace_hide_tiled_clones_recursively(trace_doc->getRoot());

    trace_doc->getRoot()->requestDisplayUpdate(SP_OBJECT_MODIFIED_FLAG);
    trace_doc->ensureUpToDate();

    trace_zoom = zoom;
}

guint32 DicCloneTiler::clonetiler_trace_pick(Geom::Rect box)
{
    if (!trace_drawing) {
        return 0;
    }

    trace_drawing->root()->setTransform(Geom::Scale(trace_zoom));
    trace_drawing->update();

    /* Item integer bbox in points */
    Geom::IntRect ibox = (box * Geom::Scale(trace_zoom)).roundOutwards();

    /* Find visible area */
    cairo_surface_t *s = cairo_image_surface_create(CAIRO_FORMAT_ARGB32, ibox.width(), ibox.height());
    Inkscape::DrawingContext dc(s, ibox.min());
    /* Render */
    trace_drawing->render(dc, ibox);
    double R = 0, G = 0, B = 0, A = 0;
    ink_cairo_surface_average_color(s, R, G, B, A);
    cairo_surface_destroy(s);

    return SP_RGBA32_F_COMPOSE (R, G, B, A);
}

void DicCloneTiler::clonetiler_trace_finish()
{
    if (trace_doc) {
        trace_doc->getRoot()->invoke_hide(trace_visionkey);
        delete trace_drawing;
        trace_doc = NULL;
        trace_drawing = NULL;
    }
}

void DicCloneTiler::clonetiler_unclump(GtkWidget */*widget*/, void *)
{
    SPDesktop *desktop = SP_ACTIVE_DESKTOP;
    if (desktop == NULL) {
        return;
    }

    Inkscape::Selection *selection = desktop->getSelection();

    // check if something is selected
    if (selection->isEmpty() || selection->itemList().size() > 1) {
        desktop->getMessageStack()->flash(Inkscape::WARNING_MESSAGE, _("Select <b>one object</b> whose tiled clones to unclump."));
        return;
    }

    SPObject *obj = selection->singleItem();
    SPObject *parent = obj->parent;

    std::vector<SPItem*> to_unclump; // not including the original

    for (SPObject *child = parent->firstChild(); child != NULL; child = child->next) {
        if (clonetiler_is_a_clone_of (child, obj)) {
            to_unclump.push_back((SPItem*)child);
        }
    }

    desktop->getDocument()->ensureUpToDate();
    reverse(to_unclump.begin(),to_unclump.end());
    unclump (to_unclump);

    DocumentUndo::done(desktop->getDocument(), SP_VERB_DIALOG_CLONETILER,
                       _("Unclump tiled clones"));
}

guint DicCloneTiler::clonetiler_number_of_clones(SPObject *obj)
{
    SPObject *parent = obj->parent;

    guint n = 0;

    for (SPObject *child = parent->firstChild(); child != NULL; child = child->next) {
        if (clonetiler_is_a_clone_of (child, obj)) {
            n ++;
        }
    }

    return n;
}

void DicCloneTiler::clonetiler_remove(GtkWidget */*widget*/, GtkWidget *dlg, bool do_undo/* = true*/)
{
    SPDesktop *desktop = SP_ACTIVE_DESKTOP;
    if (desktop == NULL) {
        return;
    }

    Inkscape::Selection *selection = desktop->getSelection();

    // check if something is selected
    if (selection->isEmpty() || selection->itemList().size() > 1) {
        desktop->getMessageStack()->flash(Inkscape::WARNING_MESSAGE, _("Select <b>one object</b> whose tiled clones to remove."));
        return;
    }

    SPObject *obj = selection->singleItem();
    SPObject *parent = obj->parent;

// remove old tiling
    GSList *to_delete = NULL;
    for (SPObject *child = parent->firstChild(); child != NULL; child = child->next) {
        if (clonetiler_is_a_clone_of (child, obj)) {
            to_delete = g_slist_prepend (to_delete, child);
        }
    }
    for (GSList *i = to_delete; i; i = i->next) {
        SPObject *obj = reinterpret_cast<SPObject *>(i->data);
        g_assert(obj != NULL);
        obj->deleteObject();
    }
    g_slist_free (to_delete);

    clonetiler_change_selection (selection, dlg);

    if (do_undo) {
        DocumentUndo::done(desktop->getDocument(), SP_VERB_DIALOG_CLONETILER,
                           _("Delete tiled clones"));
    }
}

Geom::Rect DicCloneTiler::transform_rect(Geom::Rect const &r, Geom::Affine const &m)
{
    using Geom::X;
    using Geom::Y;
    Geom::Point const p1 = r.corner(1) * m;
    Geom::Point const p2 = r.corner(2) * m;
    Geom::Point const p3 = r.corner(3) * m;
    Geom::Point const p4 = r.corner(4) * m;
    return Geom::Rect(
        Geom::Point(
            std::min(std::min(p1[X], p2[X]), std::min(p3[X], p4[X])),
            std::min(std::min(p1[Y], p2[Y]), std::min(p3[Y], p4[Y]))),
        Geom::Point(
            std::max(std::max(p1[X], p2[X]), std::max(p3[X], p4[X])),
            std::max(std::max(p1[Y], p2[Y]), std::max(p3[Y], p4[Y]))));
}

/**
Randomizes \a val by \a rand, with 0 < val < 1 and all values (including 0, 1) having the same
probability of being displaced.
 */
double DicCloneTiler::randomize01(double val, double rand)
{
    double base = MIN (val - rand, 1 - 2*rand);
    if (base < 0) {
        base = 0;
    }
    val = base + g_random_double_range (0, MIN (2 * rand, 1 - base));
    return CLAMP(val, 0, 1); // this should be unnecessary with the above provisions, but just in case...
}


void DicCloneTiler::clonetiler_apply(GtkWidget */*widget*/, GtkWidget *dlg)
{
    SPDesktop *desktop = SP_ACTIVE_DESKTOP;
    if (desktop == NULL) {
        return;
    }
    Inkscape::Preferences *prefs = Inkscape::Preferences::get();
    Inkscape::Selection *selection = desktop->getSelection();

    // check if something is selected
    if (selection->isEmpty()) {
        desktop->getMessageStack()->flash(Inkscape::WARNING_MESSAGE, _("Select an <b>object</b> to clone."));
        return;
    }

    // Check if more than one object is selected.
    if (selection->itemList().size() > 1) {
        desktop->getMessageStack()->flash(Inkscape::ERROR_MESSAGE, _("If you want to clone several objects, <b>group</b> them and <b>clone the group</b>."));
        return;
    }

    // set "busy" cursor
    desktop->setWaitingCursor();

    // set statusbar text
    GtkWidget *status = GTK_WIDGET(g_object_get_data (G_OBJECT(dlg), "status"));
    gtk_label_set_markup (GTK_LABEL(status), _("<small>Creating tiled clones...</small>"));
    gtk_widget_queue_draw(GTK_WIDGET(status));
    gdk_window_process_all_updates();

    SPObject *obj = selection->singleItem();
    if (!obj) {
        // Should never happen (empty selection checked above).
        std::cerr << "DicCloneTiler::clonetiler_apply(): No object in single item selection!!!" << std::endl;
        return;
    }
    Inkscape::XML::Node *obj_repr = obj->getRepr();
    const char *id_href = g_strdup_printf("#%s", obj_repr->attribute("id"));
    SPObject *parent = obj->parent;

    clonetiler_remove (NULL, dlg, false);

    Geom::Scale scale = desktop->getDocument()->getDocumentScale().inverse();
    double scale_units = scale[Geom::X]; // Use just x direction....

    double shiftx_per_i = 0.01 * prefs->getDoubleLimited(prefs_path + "shiftx_per_i", 0, -10000, 10000);
    double shifty_per_i = 0.01 * prefs->getDoubleLimited(prefs_path + "shifty_per_i", 0, -10000, 10000);
    double shiftx_per_j = 0.01 * prefs->getDoubleLimited(prefs_path + "shiftx_per_j", 0, -10000, 10000);
    double shifty_per_j = 0.01 * prefs->getDoubleLimited(prefs_path + "shifty_per_j", 0, -10000, 10000);
    double shiftx_rand  = 0.01 * prefs->getDoubleLimited(prefs_path + "shiftx_rand", 0, 0, 1000);
    double shifty_rand  = 0.01 * prefs->getDoubleLimited(prefs_path + "shifty_rand", 0, 0, 1000);
    double shiftx_exp   =        prefs->getDoubleLimited(prefs_path + "shiftx_exp",   1, 0, 10);
    double shifty_exp   =        prefs->getDoubleLimited(prefs_path + "shifty_exp", 1, 0, 10);
    bool   shiftx_alternate =    prefs->getBool(prefs_path + "shiftx_alternate");
    bool   shifty_alternate =    prefs->getBool(prefs_path + "shifty_alternate");
    bool   shiftx_cumulate  =    prefs->getBool(prefs_path + "shiftx_cumulate");
    bool   shifty_cumulate  =    prefs->getBool(prefs_path + "shifty_cumulate");
    bool   shiftx_excludew  =    prefs->getBool(prefs_path + "shiftx_excludew");
    bool   shifty_excludeh  =    prefs->getBool(prefs_path + "shifty_excludeh");

    double scalex_per_i = 0.01 * prefs->getDoubleLimited(prefs_path + "scalex_per_i", 0, -100, 1000);
    double scaley_per_i = 0.01 * prefs->getDoubleLimited(prefs_path + "scaley_per_i", 0, -100, 1000);
    double scalex_per_j = 0.01 * prefs->getDoubleLimited(prefs_path + "scalex_per_j", 0, -100, 1000);
    double scaley_per_j = 0.01 * prefs->getDoubleLimited(prefs_path + "scaley_per_j", 0, -100, 1000);
    double scalex_rand  = 0.01 * prefs->getDoubleLimited(prefs_path + "scalex_rand",  0, 0, 1000);
    double scaley_rand  = 0.01 * prefs->getDoubleLimited(prefs_path + "scaley_rand",  0, 0, 1000);
    double scalex_exp   =        prefs->getDoubleLimited(prefs_path + "scalex_exp",   1, 0, 10);
    double scaley_exp   =        prefs->getDoubleLimited(prefs_path + "scaley_exp",   1, 0, 10);
    double scalex_log       =    prefs->getDoubleLimited(prefs_path + "scalex_log",   0, 0, 10);
    double scaley_log       =    prefs->getDoubleLimited(prefs_path + "scaley_log",   0, 0, 10);
    bool   scalex_alternate =    prefs->getBool(prefs_path + "scalex_alternate");
    bool   scaley_alternate =    prefs->getBool(prefs_path + "scaley_alternate");
    bool   scalex_cumulate  =    prefs->getBool(prefs_path + "scalex_cumulate");
    bool   scaley_cumulate  =    prefs->getBool(prefs_path + "scaley_cumulate");

    double rotate_per_i =        prefs->getDoubleLimited(prefs_path + "rotate_per_i", 0, -180, 180);
    double rotate_per_j =        prefs->getDoubleLimited(prefs_path + "rotate_per_j", 0, -180, 180);
    double rotate_rand =  0.01 * prefs->getDoubleLimited(prefs_path + "rotate_rand", 0, 0, 100);
    bool   rotate_alternatei   = prefs->getBool(prefs_path + "rotate_alternatei");
    bool   rotate_alternatej   = prefs->getBool(prefs_path + "rotate_alternatej");
    bool   rotate_cumulatei    = prefs->getBool(prefs_path + "rotate_cumulatei");
    bool   rotate_cumulatej    = prefs->getBool(prefs_path + "rotate_cumulatej");

    double blur_per_i =   0.01 * prefs->getDoubleLimited(prefs_path + "blur_per_i", 0, 0, 100);
    double blur_per_j =   0.01 * prefs->getDoubleLimited(prefs_path + "blur_per_j", 0, 0, 100);
    bool   blur_alternatei =     prefs->getBool(prefs_path + "blur_alternatei");
    bool   blur_alternatej =     prefs->getBool(prefs_path + "blur_alternatej");
    double blur_rand =    0.01 * prefs->getDoubleLimited(prefs_path + "blur_rand", 0, 0, 100);

    double opacity_per_i = 0.01 * prefs->getDoubleLimited(prefs_path + "opacity_per_i", 0, 0, 100);
    double opacity_per_j = 0.01 * prefs->getDoubleLimited(prefs_path + "opacity_per_j", 0, 0, 100);
    bool   opacity_alternatei =   prefs->getBool(prefs_path + "opacity_alternatei");
    bool   opacity_alternatej =   prefs->getBool(prefs_path + "opacity_alternatej");
    double opacity_rand =  0.01 * prefs->getDoubleLimited(prefs_path + "opacity_rand", 0, 0, 100);

    Glib::ustring initial_color =    prefs->getString(prefs_path + "initial_color");
    double hue_per_j =        0.01 * prefs->getDoubleLimited(prefs_path + "hue_per_j", 0, -100, 100);
    double hue_per_i =        0.01 * prefs->getDoubleLimited(prefs_path + "hue_per_i", 0, -100, 100);
    double hue_rand  =        0.01 * prefs->getDoubleLimited(prefs_path + "hue_rand", 0, 0, 100);
    double saturation_per_j = 0.01 * prefs->getDoubleLimited(prefs_path + "saturation_per_j", 0, -100, 100);
    double saturation_per_i = 0.01 * prefs->getDoubleLimited(prefs_path + "saturation_per_i", 0, -100, 100);
    double saturation_rand =  0.01 * prefs->getDoubleLimited(prefs_path + "saturation_rand", 0, 0, 100);
    double lightness_per_j =  0.01 * prefs->getDoubleLimited(prefs_path + "lightness_per_j", 0, -100, 100);
    double lightness_per_i =  0.01 * prefs->getDoubleLimited(prefs_path + "lightness_per_i", 0, -100, 100);
    double lightness_rand =   0.01 * prefs->getDoubleLimited(prefs_path + "lightness_rand", 0, 0, 100);
    bool   color_alternatej = prefs->getBool(prefs_path + "color_alternatej");
    bool   color_alternatei = prefs->getBool(prefs_path + "color_alternatei");

    int    type = prefs->getInt(prefs_path + "symmetrygroup", 0);
    bool   keepbbox = prefs->getBool(prefs_path + "keepbbox", true);
    int    imax = prefs->getInt(prefs_path + "imax", 2);
    int    jmax = prefs->getInt(prefs_path + "jmax", 2);

    bool   fillrect = prefs->getBool(prefs_path + "fillrect");
    double fillwidth = scale_units*prefs->getDoubleLimited(prefs_path + "fillwidth", 50, 0, 1e6);
    double fillheight = scale_units*prefs->getDoubleLimited(prefs_path + "fillheight", 50, 0, 1e6);

    bool   dotrace = prefs->getBool(prefs_path + "dotrace");
    int    pick = prefs->getInt(prefs_path + "pick");
    bool   pick_to_presence = prefs->getBool(prefs_path + "pick_to_presence");
    bool   pick_to_size = prefs->getBool(prefs_path + "pick_to_size");
    bool   pick_to_color = prefs->getBool(prefs_path + "pick_to_color");
    bool   pick_to_opacity = prefs->getBool(prefs_path + "pick_to_opacity");
    double rand_picked = 0.01 * prefs->getDoubleLimited(prefs_path + "rand_picked", 0, 0, 100);
    bool   invert_picked = prefs->getBool(prefs_path + "invert_picked");
    double gamma_picked = prefs->getDoubleLimited(prefs_path + "gamma_picked", 0, -10, 10);

    double gapBetweenRow = scale_units*prefs->getDoubleLimited(prefs_path + "gapBetweenRow", 50, 0, 1e6);
    double gapBetweenCols = scale_units*prefs->getDoubleLimited(prefs_path + "gapBetweenCols", 50, 0, 1e6);

    SPItem *item = dynamic_cast<SPItem *>(obj);
    if (dotrace) {
        clonetiler_trace_setup (desktop->getDocument(), 1.0, item);
    }

    Geom::Point center;
    double w = 0;
    double h = 0;
    double x0 = 0;
    double y0 = 0;

    if (keepbbox &&
        obj_repr->attribute("inkscape:tile-w") &&
        obj_repr->attribute("inkscape:tile-h") &&
        obj_repr->attribute("inkscape:tile-x0") &&
        obj_repr->attribute("inkscape:tile-y0") &&
        obj_repr->attribute("inkscape:tile-cx") &&
        obj_repr->attribute("inkscape:tile-cy")) {

        double cx = 0;
        double cy = 0;
        sp_repr_get_double (obj_repr, "inkscape:tile-cx", &cx);
        sp_repr_get_double (obj_repr, "inkscape:tile-cy", &cy);
        center = Geom::Point (cx, cy);

        sp_repr_get_double (obj_repr, "inkscape:tile-w", &w);
        sp_repr_get_double (obj_repr, "inkscape:tile-h", &h);
        sp_repr_get_double (obj_repr, "inkscape:tile-x0", &x0);
        sp_repr_get_double (obj_repr, "inkscape:tile-y0", &y0);
    } else {
        bool prefs_bbox = prefs->getBool("/tools/bounding_box", false);
        SPItem::BBoxType bbox_type = ( !prefs_bbox ?
            SPItem::VISUAL_BBOX : SPItem::GEOMETRIC_BBOX );
        Geom::OptRect r = item->documentBounds(bbox_type);
        if (r) {
            w = scale_units*r->dimensions()[Geom::X];
            h = scale_units*r->dimensions()[Geom::Y];
            x0 = scale_units*r->min()[Geom::X];
            y0 = scale_units*r->min()[Geom::Y];
            center = scale_units*desktop->dt2doc(item->getCenter());

            sp_repr_set_svg_double(obj_repr, "inkscape:tile-cx", center[Geom::X]);
            sp_repr_set_svg_double(obj_repr, "inkscape:tile-cy", center[Geom::Y]);
            sp_repr_set_svg_double(obj_repr, "inkscape:tile-w", w);
            sp_repr_set_svg_double(obj_repr, "inkscape:tile-h", h);
            sp_repr_set_svg_double(obj_repr, "inkscape:tile-x0", x0);
            sp_repr_set_svg_double(obj_repr, "inkscape:tile-y0", y0);
        } else {
            center = Geom::Point(0, 0);
            w = h = 0;
            x0 = y0 = 0;
        }
    }

    Geom::Point cur(0, 0);
    Geom::Rect bbox_original (Geom::Point (x0, y0), Geom::Point (x0 + w, y0 + h));
    double perimeter_original = (w + h)/4;

    // The integers i and j are reserved for tile column and row.
    // The doubles x and y are used for coordinates
    for (int i = 0;
         fillrect?
             (fabs(cur[Geom::X]) < fillwidth && i < 200) // prevent "freezing" with too large fillrect, arbitrarily limit rows
             : (i < imax);
         i ++) {
        for (int j = 0;
             fillrect?
                 (fabs(cur[Geom::Y]) < fillheight && j < 200) // prevent "freezing" with too large fillrect, arbitrarily limit cols
                 : (j < jmax);
             j ++) {

            // Note: We create a clone at 0,0 too, right over the original, in case our clones are colored

            // Get transform from symmetry, shift, scale, rotation
            Geom::Affine orig_t = clonetiler_get_transform (type, i, j, center[Geom::X], center[Geom::Y], w, h,
                                                       shiftx_per_i,     shifty_per_i,
                                                       shiftx_per_j,     shifty_per_j,
                                                       shiftx_rand,      shifty_rand,
                                                       shiftx_exp,       shifty_exp,
                                                       shiftx_alternate, shifty_alternate,
                                                       shiftx_cumulate,  shifty_cumulate,
                                                       shiftx_excludew,  shifty_excludeh,
                                                       scalex_per_i,     scaley_per_i,
                                                       scalex_per_j,     scaley_per_j,
                                                       scalex_rand,      scaley_rand,
                                                       scalex_exp,       scaley_exp,
                                                       scalex_log,       scaley_log,
                                                       scalex_alternate, scaley_alternate,
                                                       scalex_cumulate,  scaley_cumulate,
                                                       rotate_per_i,     rotate_per_j,
                                                       rotate_rand,
                                                       rotate_alternatei, rotate_alternatej,
                                                       rotate_cumulatei,  rotate_cumulatej      );
            Geom::Affine parent_transform = (((SPItem*)item->parent)->i2doc_affine())*(item->document->getRoot()->c2p.inverse());
            Geom::Affine t = parent_transform*orig_t*parent_transform.inverse();
            cur = center * t - center;
            if (fillrect) {
                if ((cur[Geom::X] > fillwidth) || (cur[Geom::Y] > fillheight)) { // off limits
                    continue;
                }
            }

            gchar color_string[32]; *color_string = 0;

            // Color tab
            if (!initial_color.empty()) {
                guint32 rgba = sp_svg_read_color (initial_color.data(), 0x000000ff);
                float hsl[3];
                sp_color_rgb_to_hsl_floatv (hsl, SP_RGBA32_R_F(rgba), SP_RGBA32_G_F(rgba), SP_RGBA32_B_F(rgba));

                double eff_i = (color_alternatei? (i%2) : (i));
                double eff_j = (color_alternatej? (j%2) : (j));

                hsl[0] += hue_per_i * eff_i + hue_per_j * eff_j + hue_rand * g_random_double_range (-1, 1);
                double notused;
                hsl[0] = modf( hsl[0], &notused ); // Restrict to 0-1
                hsl[1] += saturation_per_i * eff_i + saturation_per_j * eff_j + saturation_rand * g_random_double_range (-1, 1);
                hsl[1] = CLAMP (hsl[1], 0, 1);
                hsl[2] += lightness_per_i * eff_i + lightness_per_j * eff_j + lightness_rand * g_random_double_range (-1, 1);
                hsl[2] = CLAMP (hsl[2], 0, 1);

                float rgb[3];
                sp_color_hsl_to_rgb_floatv (rgb, hsl[0], hsl[1], hsl[2]);
                sp_svg_write_color(color_string, sizeof(color_string), SP_RGBA32_F_COMPOSE(rgb[0], rgb[1], rgb[2], 1.0));
            }

            // Blur
            double blur = 0.0;
            {
            int eff_i = (blur_alternatei? (i%2) : (i));
            int eff_j = (blur_alternatej? (j%2) : (j));
            blur =  (blur_per_i * eff_i + blur_per_j * eff_j + blur_rand * g_random_double_range (-1, 1));
            blur = CLAMP (blur, 0, 1);
            }

            // Opacity
            double opacity = 1.0;
            {
            int eff_i = (opacity_alternatei? (i%2) : (i));
            int eff_j = (opacity_alternatej? (j%2) : (j));
            opacity = 1 - (opacity_per_i * eff_i + opacity_per_j * eff_j + opacity_rand * g_random_double_range (-1, 1));
            opacity = CLAMP (opacity, 0, 1);
            }

            // Trace tab
            if (dotrace) {
                Geom::Rect bbox_t = transform_rect (bbox_original, t*Geom::Scale(1.0/scale_units));

                guint32 rgba = clonetiler_trace_pick (bbox_t);
                float r = SP_RGBA32_R_F(rgba);
                float g = SP_RGBA32_G_F(rgba);
                float b = SP_RGBA32_B_F(rgba);
                float a = SP_RGBA32_A_F(rgba);

                float hsl[3];
                sp_color_rgb_to_hsl_floatv (hsl, r, g, b);

                gdouble val = 0;
                switch (pick) {
                case DIC_PICK_COLOR:
                    val = 1 - hsl[2]; // inverse lightness; to match other picks where black = max
                    break;
                case DIC_PICK_OPACITY:
                    val = a;
                    break;
                case DIC_PICK_R:
                    val = r;
                    break;
                case DIC_PICK_G:
                    val = g;
                    break;
                case DIC_PICK_B:
                    val = b;
                    break;
                case DIC_PICK_H:
                    val = hsl[0];
                    break;
                case DIC_PICK_S:
                    val = hsl[1];
                    break;
                case DIC_PICK_L:
                    val = 1 - hsl[2];
                    break;
                default:
                    break;
                }

                if (rand_picked > 0) {
                    val = randomize01 (val, rand_picked);
                    r = randomize01 (r, rand_picked);
                    g = randomize01 (g, rand_picked);
                    b = randomize01 (b, rand_picked);
                }

                if (gamma_picked != 0) {
                    double power;
                    if (gamma_picked > 0)
                        power = 1/(1 + fabs(gamma_picked));
                    else
                        power = 1 + fabs(gamma_picked);

                    val = pow (val, power);
                    r = pow (r, power);
                    g = pow (g, power);
                    b = pow (b, power);
                }

                if (invert_picked) {
                    val = 1 - val;
                    r = 1 - r;
                    g = 1 - g;
                    b = 1 - b;
                }

                val = CLAMP (val, 0, 1);
                r = CLAMP (r, 0, 1);
                g = CLAMP (g, 0, 1);
                b = CLAMP (b, 0, 1);

                // recompose tweaked color
                rgba = SP_RGBA32_F_COMPOSE(r, g, b, a);

                if (pick_to_presence) {
                    if (g_random_double_range (0, 1) > val) {
                        continue; // skip!
                    }
                }
                if (pick_to_size) {
                    t = parent_transform * Geom::Translate(-center[Geom::X], -center[Geom::Y])
                    * Geom::Scale (val, val) * Geom::Translate(center[Geom::X], center[Geom::Y])
                    * parent_transform.inverse() * t;
                }
                if (pick_to_opacity) {
                    opacity *= val;
                }
                if (pick_to_color) {
                    sp_svg_write_color(color_string, sizeof(color_string), rgba);
                }
            }

            if (opacity < 1e-6) { // invisibly transparent, skip
                continue;
            }

            if (fabs(t[0]) + fabs (t[1]) + fabs(t[2]) + fabs(t[3]) < 1e-6) { // too small, skip
                continue;
            }

            // Create the clone
            Inkscape::XML::Node *clone = obj_repr->document()->createElement("svg:use");
            clone->setAttribute("x", "0");
            clone->setAttribute("y", "0");
            clone->setAttribute("inkscape:tiled-clone-of", id_href);
            clone->setAttribute("xlink:href", id_href);

            Geom::Point new_center;
            bool center_set = false;
            if (obj_repr->attribute("inkscape:transform-center-x") || obj_repr->attribute("inkscape:transform-center-y")) {
                new_center = scale_units*desktop->dt2doc(item->getCenter()) * orig_t;
                center_set = true;
            }

            gchar *affinestr=sp_svg_transform_write(t);
            clone->setAttribute("transform", affinestr);
            g_free(affinestr);

            if (opacity < 1.0) {
                sp_repr_set_css_double(clone, "opacity", opacity);
            }

            if (*color_string) {
                clone->setAttribute("fill", color_string);
                clone->setAttribute("stroke", color_string);
            }

            // add the new clone to the top of the original's parent
            parent->getRepr()->appendChild(clone);

            if (blur > 0.0) {
                SPObject *clone_object = desktop->getDocument()->getObjectByRepr(clone);
                double perimeter = perimeter_original * t.descrim();
                double radius = blur * perimeter;
                // this is necessary for all newly added clones to have correct bboxes,
                // otherwise filters won't work:
                desktop->getDocument()->ensureUpToDate();
                // it's hard to figure out exact width/height of the tile without having an object
                // that we can take bbox of; however here we only need a lower bound so that blur
                // margins are not too small, and the perimeter should work
                SPFilter *constructed = new_filter_gaussian_blur(desktop->getDocument(), radius, t.descrim(), t.expansionX(), t.expansionY(), perimeter, perimeter);
                sp_style_set_property_url (clone_object, "filter", constructed, false);
            }

            if (center_set) {
                SPObject *clone_object = desktop->getDocument()->getObjectByRepr(clone);
                SPItem *item = dynamic_cast<SPItem *>(clone_object);
                if (clone_object && item) {
                    clone_object->requestDisplayUpdate(SP_OBJECT_MODIFIED_FLAG);
                    item->setCenter(desktop->doc2dt(new_center));
                    clone_object->updateRepr();
                }
            }

            Inkscape::GC::release(clone);
        }
        cur[Geom::Y] = 0;
    }

    if (dotrace) {
        clonetiler_trace_finish ();
    }

    clonetiler_change_selection (selection, dlg);

    desktop->clearWaitingCursor();

    DocumentUndo::done(desktop->getDocument(), SP_VERB_DIALOG_CLONETILER,
                       _("Create tiled clones"));
}

GtkWidget * DicCloneTiler::clonetiler_new_tab(GtkWidget *nb, const gchar *label)
{
    GtkWidget *l = gtk_label_new_with_mnemonic (label);
#if GTK_CHECK_VERSION(3,0,0)
    GtkWidget *vb = gtk_box_new(GTK_ORIENTATION_VERTICAL, VB_MARGIN);
    gtk_box_set_homogeneous(GTK_BOX(vb), FALSE);
#else
    GtkWidget *vb = gtk_vbox_new (FALSE, VB_MARGIN);
#endif
    gtk_container_set_border_width (GTK_CONTAINER (vb), VB_MARGIN);
    gtk_notebook_append_page (GTK_NOTEBOOK (nb), vb, l);
    return vb;
}

void DicCloneTiler::clonetiler_checkbox_toggled(GtkToggleButton *tb, gpointer *data)
{
    const gchar *attr = (const gchar *) data;
    Inkscape::Preferences *prefs = Inkscape::Preferences::get();
    prefs->setBool(prefs_path + attr, gtk_toggle_button_get_active(tb));
}

GtkWidget * DicCloneTiler::clonetiler_checkbox(const char *tip, const char *attr)
{
#if GTK_CHECK_VERSION(3,0,0)
    GtkWidget *hb = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, VB_MARGIN);
    gtk_box_set_homogeneous(GTK_BOX(hb), FALSE);
#else
    GtkWidget *hb = gtk_hbox_new(FALSE, VB_MARGIN);
#endif

    GtkWidget *b = gtk_check_button_new ();
    gtk_widget_set_tooltip_text (b, tip);

    Inkscape::Preferences *prefs = Inkscape::Preferences::get();
    bool value = prefs->getBool(prefs_path + attr);
    gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON(b), value);

    gtk_box_pack_end (GTK_BOX (hb), b, FALSE, TRUE, 0);
    g_signal_connect ( G_OBJECT (b), "clicked",
                         G_CALLBACK (clonetiler_checkbox_toggled), (gpointer) attr);

    g_object_set_data (G_OBJECT(b), "uncheckable", GINT_TO_POINTER(TRUE));

    return hb;
}

void DicCloneTiler::clonetiler_value_changed(GtkAdjustment *adj, gpointer data)
{
    Inkscape::Preferences *prefs = Inkscape::Preferences::get();
    const gchar *pref = (const gchar *) data;
    prefs->setDouble(prefs_path + pref, gtk_adjustment_get_value (adj));
}

GtkWidget * DicCloneTiler::clonetiler_spinbox(const char *tip, const char *attr, double lower, double upper, const gchar *suffix, bool exponent/* = false*/)
{
#if GTK_CHECK_VERSION(3,0,0)
    GtkWidget *hb = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
    gtk_box_set_homogeneous(GTK_BOX(hb), FALSE);
#else
    GtkWidget *hb = gtk_hbox_new(FALSE, 0);
#endif

    {
#if WITH_GTKMM_3_0
        Glib::RefPtr<Gtk::Adjustment> a;
        if (exponent) {
            a = Gtk::Adjustment::create(1.0, lower, upper, 0.01, 0.05, 0);
        } else {
            a = Gtk::Adjustment::create(0.0, lower, upper, 0.1, 0.5, 0);
        }
#else
        Gtk::Adjustment *a;
        if (exponent) {
            a = new Gtk::Adjustment (1.0, lower, upper, 0.01, 0.05, 0);
        } else {
            a = new Gtk::Adjustment (0.0, lower, upper, 0.1, 0.5, 0);
        }
#endif

        Inkscape::UI::Widget::SpinButton *sb;
#if WITH_GTKMM_3_0
        if (exponent) {
            sb = new Inkscape::UI::Widget::SpinButton(a, 0.01, 2);
        } else {
            sb = new Inkscape::UI::Widget::SpinButton(a, 0.1, 1);
	}
#else
        if (exponent) {
            sb = new Inkscape::UI::Widget::SpinButton (*a, 0.01, 2);
        } else {
            sb = new Inkscape::UI::Widget::SpinButton (*a, 0.1, 1);
        }
#endif

        sb->set_tooltip_text (tip);
        sb->set_width_chars (5);
        sb->set_digits(3);
        gtk_box_pack_start (GTK_BOX (hb), GTK_WIDGET(sb->gobj()), FALSE, FALSE, SB_MARGIN);

        Inkscape::Preferences *prefs = Inkscape::Preferences::get();
        double value = prefs->getDoubleLimited(prefs_path + attr, exponent? 1.0 : 0.0, lower, upper);
        a->set_value (value);
        // TODO: C++ification
        g_signal_connect(G_OBJECT(a->gobj()), "value_changed",
                           G_CALLBACK(clonetiler_value_changed), (gpointer) attr);

        if (exponent) {
            sb->set_data ("oneable", GINT_TO_POINTER(TRUE));
        } else {
            sb->set_data ("zeroable", GINT_TO_POINTER(TRUE));
        }
    }

    {
        GtkWidget *l = gtk_label_new ("");
        gtk_label_set_markup (GTK_LABEL(l), suffix);
#if GTK_CHECK_VERSION(3,0,0)
        gtk_widget_set_halign(l, GTK_ALIGN_END);
        gtk_widget_set_valign(l, GTK_ALIGN_START);
#else
        gtk_misc_set_alignment (GTK_MISC (l), 1.0, 0);
#endif
        gtk_box_pack_start (GTK_BOX (hb), l, FALSE, FALSE, 0);
    }

    return hb;
}

void DicCloneTiler::clonetiler_symgroup_changed(GtkComboBox *cb, gpointer /*data*/)
{
    Inkscape::Preferences *prefs = Inkscape::Preferences::get();
    gint group_new = gtk_combo_box_get_active (cb);
    prefs->setInt(prefs_path + "symmetrygroup", group_new);
}

void DicCloneTiler::clonetiler_xy_changed(GtkAdjustment *adj, gpointer data)
{
    Inkscape::Preferences *prefs = Inkscape::Preferences::get();
    const gchar *pref = (const gchar *) data;
    prefs->setInt(prefs_path + pref, (int) floor(gtk_adjustment_get_value (adj) + 0.5));
}

void DicCloneTiler::clonetiler_keep_bbox_toggled(GtkToggleButton *tb, gpointer /*data*/)
{
    Inkscape::Preferences *prefs = Inkscape::Preferences::get();
    prefs->setBool(prefs_path + "keepbbox", gtk_toggle_button_get_active(tb));
}

void DicCloneTiler::clonetiler_pick_to(GtkToggleButton *tb, gpointer data)
{
    Inkscape::Preferences *prefs = Inkscape::Preferences::get();
    const gchar *pref = (const gchar *) data;
    prefs->setBool(prefs_path + pref, gtk_toggle_button_get_active(tb));
}


void DicCloneTiler::clonetiler_reset_recursive(GtkWidget *w)
{
    if (w && G_IS_OBJECT(w)) {
        {
            int r = GPOINTER_TO_INT (g_object_get_data(G_OBJECT(w), "zeroable"));
            if (r && GTK_IS_SPIN_BUTTON(w)) { // spinbutton
                GtkAdjustment *a = gtk_spin_button_get_adjustment (GTK_SPIN_BUTTON(w));
                gtk_adjustment_set_value (a, 0);
            }
        }
        {
            int r = GPOINTER_TO_INT (g_object_get_data(G_OBJECT(w), "oneable"));
            if (r && GTK_IS_SPIN_BUTTON(w)) { // spinbutton
                GtkAdjustment *a = gtk_spin_button_get_adjustment (GTK_SPIN_BUTTON(w));
                gtk_adjustment_set_value (a, 1);
            }
        }
        {
            int r = GPOINTER_TO_INT (g_object_get_data(G_OBJECT(w), "uncheckable"));
            if (r && GTK_IS_TOGGLE_BUTTON(w)) { // checkbox
                gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON(w), FALSE);
            }
        }
    }

    if (GTK_IS_CONTAINER(w)) {
        GList *ch = gtk_container_get_children (GTK_CONTAINER(w));
        for (GList *i = ch; i != NULL; i = i->next) {
            clonetiler_reset_recursive (GTK_WIDGET(i->data));
        }
        g_list_free (ch);
    }
}

void DicCloneTiler::clonetiler_reset(GtkWidget */*widget*/, GtkWidget *dlg)
{
    clonetiler_reset_recursive (dlg);
}

void DicCloneTiler::clonetiler_table_attach(GtkWidget *table, GtkWidget *widget, float align, int row, int col)
{
#if GTK_CHECK_VERSION(3,0,0)
    gtk_widget_set_halign(widget, GTK_ALIGN_FILL);
    gtk_widget_set_valign(widget, GTK_ALIGN_START);
    gtk_grid_attach(GTK_GRID(table), widget, col, row, 1, 1);
#else
    GtkWidget *a = gtk_alignment_new (align, 0, 0, 0);
    gtk_container_add(GTK_CONTAINER(a), widget);
    gtk_table_attach ( GTK_TABLE (table), a, col, col + 1, row, row + 1, GTK_FILL, (GtkAttachOptions)0, 0, 0 );
#endif
}

GtkWidget * DicCloneTiler::clonetiler_table_x_y_rand(int values)
{
#if GTK_CHECK_VERSION(3,0,0)
    GtkWidget *table = gtk_grid_new();
    gtk_grid_set_row_spacing(GTK_GRID(table), 6);
    gtk_grid_set_column_spacing(GTK_GRID(table), 8);
#else
    GtkWidget *table = gtk_table_new (values + 2, 5, FALSE);
    gtk_table_set_row_spacings (GTK_TABLE (table), 6);
    gtk_table_set_col_spacings (GTK_TABLE (table), 8);
#endif

    gtk_container_set_border_width (GTK_CONTAINER (table), VB_MARGIN);

    {
#if GTK_CHECK_VERSION(3,0,0)
	GtkWidget *hb = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
	gtk_box_set_homogeneous(GTK_BOX(hb), FALSE);
#else
        GtkWidget *hb = gtk_hbox_new (FALSE, 0);
#endif

        GtkWidget *i = sp_icon_new (Inkscape::ICON_SIZE_DECORATION, INKSCAPE_ICON("object-rows"));
        gtk_box_pack_start (GTK_BOX (hb), i, FALSE, FALSE, 2);

        GtkWidget *l = gtk_label_new ("");
        gtk_label_set_markup (GTK_LABEL(l), _("<small>Per row:</small>"));
        gtk_box_pack_start (GTK_BOX (hb), l, FALSE, FALSE, 2);

        clonetiler_table_attach (table, hb, 0, 1, 2);
    }

    {
#if GTK_CHECK_VERSION(3,0,0)
	GtkWidget *hb = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
	gtk_box_set_homogeneous(GTK_BOX(hb), FALSE);
#else
        GtkWidget *hb = gtk_hbox_new (FALSE, 0);
#endif

        GtkWidget *i = sp_icon_new (Inkscape::ICON_SIZE_DECORATION, INKSCAPE_ICON("object-columns"));
        gtk_box_pack_start (GTK_BOX (hb), i, FALSE, FALSE, 2);

        GtkWidget *l = gtk_label_new ("");
        gtk_label_set_markup (GTK_LABEL(l), _("<small>Per column:</small>"));
        gtk_box_pack_start (GTK_BOX (hb), l, FALSE, FALSE, 2);

        clonetiler_table_attach (table, hb, 0, 1, 3);
    }

    {
        GtkWidget *l = gtk_label_new ("");
        gtk_label_set_markup (GTK_LABEL(l), _("<small>Randomize:</small>"));
        clonetiler_table_attach (table, l, 0, 1, 4);
    }

    return table;
}

void DicCloneTiler::clonetiler_pick_switched(GtkToggleButton */*tb*/, gpointer data)
{
    Inkscape::Preferences *prefs = Inkscape::Preferences::get();
    guint v = GPOINTER_TO_INT (data);
    prefs->setInt(prefs_path + "pick", v);
}


void DicCloneTiler::clonetiler_switch_to_create(GtkToggleButton * /*tb*/, GtkWidget *dlg)
{
    GtkWidget *rowscols = GTK_WIDGET(g_object_get_data (G_OBJECT(dlg), "rowscols"));
    GtkWidget *widthheight = GTK_WIDGET(g_object_get_data (G_OBJECT(dlg), "widthheight"));

    if (rowscols) {
        gtk_widget_set_sensitive (rowscols, TRUE);
    }
    if (widthheight) {
        gtk_widget_set_sensitive (widthheight, FALSE);
    }

    Inkscape::Preferences *prefs = Inkscape::Preferences::get();
    prefs->setBool(prefs_path + "fillrect", false);
}


void DicCloneTiler::clonetiler_switch_to_fill(GtkToggleButton * /*tb*/, GtkWidget *dlg)
{
    GtkWidget *rowscols = GTK_WIDGET(g_object_get_data (G_OBJECT(dlg), "rowscols"));
    GtkWidget *widthheight = GTK_WIDGET(g_object_get_data (G_OBJECT(dlg), "widthheight"));

    if (rowscols) {
        gtk_widget_set_sensitive (rowscols, FALSE);
    }
    if (widthheight) {
        gtk_widget_set_sensitive (widthheight, TRUE);
    }

    Inkscape::Preferences *prefs = Inkscape::Preferences::get();
    prefs->setBool(prefs_path + "fillrect", true);
}

void DicCloneTiler::clonetiler_fill_width_changed(GtkAdjustment *adj, Inkscape::UI::Widget::UnitMenu *u)
{
    gdouble const raw_dist = gtk_adjustment_get_value (adj);
    Inkscape::Util::Unit const *unit = u->getUnit();
    gdouble const pixels = Inkscape::Util::Quantity::convert(raw_dist, unit, "px");

    Inkscape::Preferences *prefs = Inkscape::Preferences::get();
    prefs->setDouble(prefs_path + "fillwidth", pixels);
}

void DicCloneTiler::clonetiler_fill_height_changed(GtkAdjustment *adj, Inkscape::UI::Widget::UnitMenu *u)
{
    gdouble const raw_dist = gtk_adjustment_get_value (adj);
    Inkscape::Util::Unit const *unit = u->getUnit();
    gdouble const pixels = Inkscape::Util::Quantity::convert(raw_dist, unit, "px");

    Inkscape::Preferences *prefs = Inkscape::Preferences::get();
    prefs->setDouble(prefs_path + "fillheight", pixels);
}

void DicCloneTiler::clonetiler_unit_changed()
{
    Inkscape::Preferences *prefs = Inkscape::Preferences::get();
    gdouble width_pixels = prefs->getDouble(prefs_path + "fillwidth");
    gdouble height_pixels = prefs->getDouble(prefs_path + "fillheight");

    Inkscape::Util::Unit const *unit = unit_menu->getUnit();

    gdouble width_value = Inkscape::Util::Quantity::convert(width_pixels, "px", unit);
    gdouble height_value = Inkscape::Util::Quantity::convert(height_pixels, "px", unit);
    gtk_adjustment_set_value(fill_width->gobj(), width_value);
    gtk_adjustment_set_value(fill_height->gobj(), height_value);
}

void DicCloneTiler::clonetiler_do_pick_toggled(GtkToggleButton *tb, GtkWidget *dlg)
{
    GtkWidget *vvb = GTK_WIDGET(g_object_get_data (G_OBJECT(dlg), "dotrace"));

    Inkscape::Preferences *prefs = Inkscape::Preferences::get();
    prefs->setBool(prefs_path + "dotrace", gtk_toggle_button_get_active (tb));

    if (vvb) {
        gtk_widget_set_sensitive (vvb, gtk_toggle_button_get_active (tb));
    }
}

void DicCloneTiler::show_page_trace()
{
    gtk_notebook_set_current_page(GTK_NOTEBOOK(nb),6);
    gtk_toggle_button_set_active ((GtkToggleButton *) b, false);
}

GtkWidget * DicCloneTiler::dicclonetiler_number_of_row_col_spinbox(const char *tip, const char *attr, int lower, int upper)
{

#if GTK_CHECK_VERSION(3,0,0)
    GtkWidget *hb = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
    gtk_box_set_homogeneous(GTK_BOX(hb), FALSE);
#else
    GtkWidget *hb = gtk_hbox_new(FALSE, 0);
#endif
    {
#if WITH_GTKMM_3_0
        Glib::RefPtr<Gtk::Adjustment> a;
        a = Gtk::Adjustment::create(0.0, lower, upper, 1, 10, 0);
#else
        Gtk::Adjustment *a;
        a = new Gtk::Adjustment (0.0, lower, upper, 1, 10, 0);
#endif
        Inkscape::Preferences *prefs = Inkscape::Preferences::get();
        prefs->setInt(prefs_path + attr, 2);
        int value = prefs->getInt(prefs_path + attr, 2);
        a->set_value (value);

        Inkscape::UI::Widget::SpinButton *sb;
#if WITH_GTKMM_3_0
        sb = new Inkscape::UI::Widget::SpinButton(a, 1.0, 0);
#else
        sb = new Inkscape::UI::Widget::SpinButton (*a, 1.0, 0);
#endif
        sb->set_tooltip_text (tip);
        sb->set_width_chars (5);
        sb->set_digits(0);
        gtk_box_pack_start (GTK_BOX (hb), GTK_WIDGET(sb->gobj()), FALSE, FALSE, SB_MARGIN);

        // TODO: C++ification
        g_signal_connect(G_OBJECT(a->gobj()), "value_changed",
                         	 G_CALLBACK(dicclonetiler_number_of_row_col_changed), (gpointer) attr);
    }
    return hb;
}

/**
 * Function to keep track of changes for setting number of rows/cols
 * in the repeat pattern
 */

void DicCloneTiler::dicclonetiler_number_of_row_col_changed(GtkAdjustment *adj, gpointer data)
{

    Inkscape::Preferences *prefs = Inkscape::Preferences::get();
    const gchar *pref = (const gchar *) data;
    prefs->setInt(prefs_path + pref, (int) floor(gtk_adjustment_get_value (adj) + 0.5));

}

/**
 * Settings for row gap, if to be set in the pattern
 */

GtkWidget * DicCloneTiler::dicclonetiler_row_gap_spinbox()
{
	Inkscape::Preferences *prefs = Inkscape::Preferences::get();

	unit_menu = new Inkscape::UI::Widget::UnitMenu();
	unit_menu->setUnitType(Inkscape::Util::UNIT_TYPE_LINEAR);
	unit_menu->setUnit(SP_ACTIVE_DESKTOP->getNamedView()->display_units->abbr);
	unitChangedConn = unit_menu->signal_changed().connect(sigc::mem_fun(*this, &DicCloneTiler::dicclonetiler_unit_row_changed));

#if GTK_CHECK_VERSION(3,0,0)
    GtkWidget *hb = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
    gtk_box_set_homogeneous(GTK_BOX(hb), FALSE);
#else
    GtkWidget *hb = gtk_hbox_new(FALSE, 0);
#endif

    {
       // Row Gap Spinbutton

#if WITH_GTKMM_3_0
    	rowDistance = Gtk::Adjustment::create(0.0, -1e6, 1e6, 1.0, 10.0, 0);
#else
    	rowDistance = new Gtk::Adjustment (0.0, -1e6, 1e6, 1.0, 10.0, 0);
#endif
     	double value = prefs->getDouble(prefs_path + "gapBetweenRow");
     	Inkscape::Util::Unit const *unit = unit_menu->getUnit();
    	gdouble const units = Inkscape::Util::Quantity::convert(value, "px", unit);
    	rowDistance->set_value (units);
    	prefs->setDouble(prefs_path + "gapBetweenRow",0);

#if WITH_GTKMM_3_0
		rowDistanceSpinButton = new Inkscape::UI::Widget::SpinButton(rowDistance, 1.0, 2);
#else
		rowDistanceSpinButton = new Inkscape::UI::Widget::SpinButton (*rowDistance, 1.0, 2);
#endif
		rowDistanceSpinButton->set_tooltip_text ("Gap between the rows");
		rowDistanceSpinButton->set_width_chars (5);
		rowDistanceSpinButton->set_digits (3);
		gtk_box_pack_start (GTK_BOX (hb), GTK_WIDGET(rowDistanceSpinButton->gobj()), TRUE, TRUE, 0);
                        // TODO: C++ification
		g_signal_connect(G_OBJECT(rowDistance->gobj()), "value_changed",
    					 G_CALLBACK(dicclonetiler_row_gap_value_changed), unit_menu);

    	gtk_box_pack_start (GTK_BOX (hb), GTK_WIDGET(unit_menu->gobj()), FALSE, FALSE, 0);

    }
    return hb;
}

/**
 * Utility to maintain the control the gap between rows in a pattern
 */

void DicCloneTiler::dicclonetiler_row_gap_value_changed(GtkAdjustment *adj, Inkscape::UI::Widget::UnitMenu *u)
{
	gdouble const raw_dist = gtk_adjustment_get_value (adj);
	Inkscape::Util::Unit const *unit = u->getUnit();
	gdouble const pixels = Inkscape::Util::Quantity::convert(raw_dist, unit, "px");

	Inkscape::Preferences *prefs = Inkscape::Preferences::get();
	prefs->setDouble(prefs_path + "gapBetweenRow", pixels);

}

/**
 * Function to carry out conversions between row units of measurement
 */

void DicCloneTiler::dicclonetiler_unit_row_changed()
{

	Inkscape::Preferences *prefs = Inkscape::Preferences::get();
	gdouble gap_row_pixels = prefs->getDouble(prefs_path + "gapBetweenRow");

	Inkscape::Util::Unit const *unit = unit_menu->getUnit();
	gdouble gap_row_value = Inkscape::Util::Quantity::convert(gap_row_pixels, "px", unit);

	gtk_adjustment_set_value(rowDistance->gobj(), gap_row_value);

}

/**
 * Settings for column gap, if to be set in the pattern
 */

GtkWidget * DicCloneTiler::dicclonetiler_col_gap_spinbox()
{

	Inkscape::Preferences *prefs = Inkscape::Preferences::get();

	unit_col_menu = new Inkscape::UI::Widget::UnitMenu();
	unit_col_menu->setUnitType(Inkscape::Util::UNIT_TYPE_LINEAR);
	unit_col_menu->setUnit(SP_ACTIVE_DESKTOP->getNamedView()->display_units->abbr);
	unitColChangedConn = unit_col_menu->signal_changed().connect(sigc::mem_fun(*this, &DicCloneTiler::dicclonetiler_unit_col_changed));

#if GTK_CHECK_VERSION(3,0,0)
    GtkWidget *hb = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
    gtk_box_set_homogeneous(GTK_BOX(hb), FALSE);
#else
    GtkWidget *hb = gtk_hbox_new(FALSE, 0);
#endif

    {
         // Col Gap Spinbutton
#if WITH_GTKMM_3_0
    	colDistance = Gtk::Adjustment::create(0.0, -1e6, 1e6, 1.0, 10.0, 0);
#else
    	colDistance = new Gtk::Adjustment (0.0, -1e6, 1e6, 1.0, 10.0, 0);
#endif
     	double value = prefs->getDouble(prefs_path + "gapBetweenCols");
     	Inkscape::Util::Unit const *unit = unit_col_menu->getUnit();
    	gdouble const units = Inkscape::Util::Quantity::convert(value, "px", unit);
    	colDistance->set_value (units);
    	prefs->setDouble(prefs_path + "gapBetweenCols",0);

#if WITH_GTKMM_3_0
		colDistanceSpinButton = new Inkscape::UI::Widget::SpinButton(colDistance, 1.0, 2);
#else
		colDistanceSpinButton = new Inkscape::UI::Widget::SpinButton (*colDistance, 1.0, 2);
#endif
		colDistanceSpinButton->set_tooltip_text ("Gap between the cols");
		colDistanceSpinButton->set_width_chars (5);
		colDistanceSpinButton->set_digits (3);
		gtk_box_pack_start (GTK_BOX (hb), GTK_WIDGET(colDistanceSpinButton->gobj()), TRUE, TRUE, 0);
                        // TODO: C++ification
    	g_signal_connect(G_OBJECT(colDistance->gobj()), "value_changed",
    					 G_CALLBACK(dicclonetiler_col_gap_value_changed), unit_col_menu);

    	gtk_box_pack_start (GTK_BOX (hb), GTK_WIDGET(unit_col_menu->gobj()), FALSE, FALSE, 0);

    }
    return hb;
}

/**
 * Function to carry out conversions between col units of measurement
 */

void DicCloneTiler::dicclonetiler_unit_col_changed()
{

	Inkscape::Preferences *prefs = Inkscape::Preferences::get();
	gdouble gap_col_pixels = prefs->getDouble(prefs_path + "gapBetweenCols");

	Inkscape::Util::Unit const *unit = unit_col_menu->getUnit();
	gdouble gap_col_value = Inkscape::Util::Quantity::convert(gap_col_pixels, "px", unit);

	gtk_adjustment_set_value(colDistance->gobj(), gap_col_value);

}

/**
 * Utility to maintain the control the gap between columns in a pattern
 */

void DicCloneTiler::dicclonetiler_col_gap_value_changed(GtkAdjustment *adj, Inkscape::UI::Widget::UnitMenu *u)
{
	gdouble const raw_dist = gtk_adjustment_get_value (adj);
	Inkscape::Util::Unit const *unit = u->getUnit();
	gdouble const pixels = Inkscape::Util::Quantity::convert(raw_dist, unit, "px");

	Inkscape::Preferences *prefs = Inkscape::Preferences::get();
	prefs->setDouble(prefs_path + "gapBetweenCols", pixels);

}

/**
 * Main function to generate the repeat patterns as per the
 * user inputs taken on DIC Shift tab and DIC Rotation tab
 */

void DicCloneTiler::dicclonetiler_apply(GtkWidget */*widget*/, GtkWidget *dlg)
{

	SPDesktop *desktop = SP_ACTIVE_DESKTOP;
    if (desktop == NULL) {
        return;
    }
    Inkscape::Preferences *prefs = Inkscape::Preferences::get();
    Inkscape::Selection *selection = desktop->getSelection();

    // check if something is selected
    if (selection->isEmpty()) {
        desktop->getMessageStack()->flash(Inkscape::WARNING_MESSAGE, _("Select an <b>object</b> to clone."));
        return;
    }

    // Check if more than one object is selected.
    if (selection->itemList().size() > 1) {
        desktop->getMessageStack()->flash(Inkscape::ERROR_MESSAGE, _("If you want to clone several objects, <b>group</b> them and <b>clone the group</b>."));
        return;
    }

    // set "busy" cursor
    desktop->setWaitingCursor();

    // set statusbar text
    GtkWidget *status = GTK_WIDGET(g_object_get_data (G_OBJECT(dlg), "status"));
    gtk_label_set_markup (GTK_LABEL(status), _("<small>Creating DIC tiled clones...</small>"));
    gtk_widget_queue_draw(GTK_WIDGET(status));
    gdk_window_process_all_updates();

    SPObject *obj = selection->singleItem();
    if (!obj) {
        // Should never happen (empty selection checked above).
        std::cerr << "DicCloneTiler::dicclonetiler_apply(): No object in single item selection!!!" << std::endl;
        return;
    }
    Inkscape::XML::Node *obj_repr = obj->getRepr();
    const char *id_href = g_strdup_printf("#%s", obj_repr->attribute("id"));
    SPObject *parent = obj->parent;

    clonetiler_remove (NULL, dlg, false);

    Geom::Scale scale = desktop->getDocument()->getDocumentScale().inverse();
    double scale_units = scale[Geom::X]; // Use just x direction....

    double shiftx_per_i = 0.01 * prefs->getDoubleLimited(prefs_path + "shiftx_per_i", 0, -10000, 10000);
    double shifty_per_i = 0.01 * prefs->getDoubleLimited(prefs_path + "shifty_per_i", 0, -10000, 10000);
    double shiftx_per_j = 0.01 * prefs->getDoubleLimited(prefs_path + "shiftx_per_j", 0, -10000, 10000);
    double shifty_per_j = 0.01 * prefs->getDoubleLimited(prefs_path + "shifty_per_j", 0, -10000, 10000);
    double shiftx_rand  = 0.01 * prefs->getDoubleLimited(prefs_path + "shiftx_rand", 0, 0, 1000);
    double shifty_rand  = 0.01 * prefs->getDoubleLimited(prefs_path + "shifty_rand", 0, 0, 1000);
    double shiftx_exp   =        prefs->getDoubleLimited(prefs_path + "shiftx_exp",   1, 0, 10);
    double shifty_exp   =        prefs->getDoubleLimited(prefs_path + "shifty_exp", 1, 0, 10);
    bool   shiftx_alternate =    prefs->getBool(prefs_path + "shiftx_alternate");
    bool   shifty_alternate =    prefs->getBool(prefs_path + "shifty_alternate");
    bool   shiftx_cumulate  =    prefs->getBool(prefs_path + "shiftx_cumulate");
    bool   shifty_cumulate  =    prefs->getBool(prefs_path + "shifty_cumulate");
    bool   shiftx_excludew  =    prefs->getBool(prefs_path + "shiftx_excludew");
    bool   shifty_excludeh  =    prefs->getBool(prefs_path + "shifty_excludeh");

    double scalex_per_i = 0.01 * prefs->getDoubleLimited(prefs_path + "scalex_per_i", 0, -100, 1000);
    double scaley_per_i = 0.01 * prefs->getDoubleLimited(prefs_path + "scaley_per_i", 0, -100, 1000);
    double scalex_per_j = 0.01 * prefs->getDoubleLimited(prefs_path + "scalex_per_j", 0, -100, 1000);
    double scaley_per_j = 0.01 * prefs->getDoubleLimited(prefs_path + "scaley_per_j", 0, -100, 1000);
    double scalex_rand  = 0.01 * prefs->getDoubleLimited(prefs_path + "scalex_rand",  0, 0, 1000);
    double scaley_rand  = 0.01 * prefs->getDoubleLimited(prefs_path + "scaley_rand",  0, 0, 1000);
    double scalex_exp   =        prefs->getDoubleLimited(prefs_path + "scalex_exp",   1, 0, 10);
    double scaley_exp   =        prefs->getDoubleLimited(prefs_path + "scaley_exp",   1, 0, 10);
    double scalex_log       =    prefs->getDoubleLimited(prefs_path + "scalex_log",   0, 0, 10);
    double scaley_log       =    prefs->getDoubleLimited(prefs_path + "scaley_log",   0, 0, 10);
    bool   scalex_alternate =    prefs->getBool(prefs_path + "scalex_alternate");
    bool   scaley_alternate =    prefs->getBool(prefs_path + "scaley_alternate");
    bool   scalex_cumulate  =    prefs->getBool(prefs_path + "scalex_cumulate");
    bool   scaley_cumulate  =    prefs->getBool(prefs_path + "scaley_cumulate");

    double rotate_per_i =        prefs->getDoubleLimited(prefs_path + "rotate_per_i", 0, -180, 180);
    double rotate_per_j =        prefs->getDoubleLimited(prefs_path + "rotate_per_j", 0, -180, 180);
    double rotate_rand =  0.01 * prefs->getDoubleLimited(prefs_path + "rotate_rand", 0, 0, 100);
    bool   rotate_alternatei   = prefs->getBool(prefs_path + "rotate_alternatei");
    bool   rotate_alternatej   = prefs->getBool(prefs_path + "rotate_alternatej");
    bool   rotate_cumulatei    = prefs->getBool(prefs_path + "rotate_cumulatei");
    bool   rotate_cumulatej    = prefs->getBool(prefs_path + "rotate_cumulatej");

    double blur_per_i =   0.01 * prefs->getDoubleLimited(prefs_path + "blur_per_i", 0, 0, 100);
    double blur_per_j =   0.01 * prefs->getDoubleLimited(prefs_path + "blur_per_j", 0, 0, 100);
    bool   blur_alternatei =     prefs->getBool(prefs_path + "blur_alternatei");
    bool   blur_alternatej =     prefs->getBool(prefs_path + "blur_alternatej");
    double blur_rand =    0.01 * prefs->getDoubleLimited(prefs_path + "blur_rand", 0, 0, 100);

    double opacity_per_i = 0.01 * prefs->getDoubleLimited(prefs_path + "opacity_per_i", 0, 0, 100);
    double opacity_per_j = 0.01 * prefs->getDoubleLimited(prefs_path + "opacity_per_j", 0, 0, 100);
    bool   opacity_alternatei =   prefs->getBool(prefs_path + "opacity_alternatei");
    bool   opacity_alternatej =   prefs->getBool(prefs_path + "opacity_alternatej");
    double opacity_rand =  0.01 * prefs->getDoubleLimited(prefs_path + "opacity_rand", 0, 0, 100);

    Glib::ustring initial_color =    prefs->getString(prefs_path + "initial_color");
    double hue_per_j =        0.01 * prefs->getDoubleLimited(prefs_path + "hue_per_j", 0, -100, 100);
    double hue_per_i =        0.01 * prefs->getDoubleLimited(prefs_path + "hue_per_i", 0, -100, 100);
    double hue_rand  =        0.01 * prefs->getDoubleLimited(prefs_path + "hue_rand", 0, 0, 100);
    double saturation_per_j = 0.01 * prefs->getDoubleLimited(prefs_path + "saturation_per_j", 0, -100, 100);
    double saturation_per_i = 0.01 * prefs->getDoubleLimited(prefs_path + "saturation_per_i", 0, -100, 100);
    double saturation_rand =  0.01 * prefs->getDoubleLimited(prefs_path + "saturation_rand", 0, 0, 100);
    double lightness_per_j =  0.01 * prefs->getDoubleLimited(prefs_path + "lightness_per_j", 0, -100, 100);
    double lightness_per_i =  0.01 * prefs->getDoubleLimited(prefs_path + "lightness_per_i", 0, -100, 100);
    double lightness_rand =   0.01 * prefs->getDoubleLimited(prefs_path + "lightness_rand", 0, 0, 100);
    bool   color_alternatej = prefs->getBool(prefs_path + "color_alternatej");
    bool   color_alternatei = prefs->getBool(prefs_path + "color_alternatei");

    int    type = prefs->getInt(prefs_path + "symmetrygroup", 0);
    //bool   keepbbox = prefs->getBool(prefs_path + "keepbbox", true);
    bool   keepbbox = prefs->getBool(prefs_path + "keepbbox", false);
    int    imax = prefs->getInt(prefs_path + "imax", 2);
    int    jmax = prefs->getInt(prefs_path + "jmax", 2);


    bool   fillrect = prefs->getBool(prefs_path + "fillrect");
    double fillwidth = scale_units*prefs->getDoubleLimited(prefs_path + "fillwidth", 50, 0, 1e6);
    double fillheight = scale_units*prefs->getDoubleLimited(prefs_path + "fillheight", 50, 0, 1e6);

    bool   dotrace = prefs->getBool(prefs_path + "dotrace");
    int    pick = prefs->getInt(prefs_path + "pick");
    bool   pick_to_presence = prefs->getBool(prefs_path + "pick_to_presence");
    bool   pick_to_size = prefs->getBool(prefs_path + "pick_to_size");
    bool   pick_to_color = prefs->getBool(prefs_path + "pick_to_color");
    bool   pick_to_opacity = prefs->getBool(prefs_path + "pick_to_opacity");
    double rand_picked = 0.01 * prefs->getDoubleLimited(prefs_path + "rand_picked", 0, 0, 100);
    bool   invert_picked = prefs->getBool(prefs_path + "invert_picked");
    double gamma_picked = prefs->getDoubleLimited(prefs_path + "gamma_picked", 0, -10, 10);

    /**
     * gapBetweenRow & gapBetweenCols are place holders for
     * maintaining internally in the preferences settings     *
     */

    double gapBetweenRow = scale_units*prefs->getDoubleLimited(prefs_path + "gapBetweenRow", 50, 0, 1e6);
    double gapBetweenCols = scale_units*prefs->getDoubleLimited(prefs_path + "gapBetweenCols", 50, 0, 1e6);

    SPItem *item = dynamic_cast<SPItem *>(obj);
    if (dotrace) {
        clonetiler_trace_setup (desktop->getDocument(), 1.0, item);
    }

    Geom::Point center;
    double w = 0;
    double h = 0;
    double x0 = 0;
    double y0 = 0;

    if (keepbbox &&
        obj_repr->attribute("inkscape:tile-w") &&
        obj_repr->attribute("inkscape:tile-h") &&
        obj_repr->attribute("inkscape:tile-x0") &&
        obj_repr->attribute("inkscape:tile-y0") &&
        obj_repr->attribute("inkscape:tile-cx") &&
        obj_repr->attribute("inkscape:tile-cy")) {

        double cx = 0;
        double cy = 0;
        sp_repr_get_double (obj_repr, "inkscape:tile-cx", &cx);
        sp_repr_get_double (obj_repr, "inkscape:tile-cy", &cy);
        center = Geom::Point (cx, cy);

        sp_repr_get_double (obj_repr, "inkscape:tile-w", &w);
        sp_repr_get_double (obj_repr, "inkscape:tile-h", &h);
        sp_repr_get_double (obj_repr, "inkscape:tile-x0", &x0);
        sp_repr_get_double (obj_repr, "inkscape:tile-y0", &y0);
    } else {
        bool prefs_bbox = prefs->getBool("/tools/bounding_box", false);
        SPItem::BBoxType bbox_type = ( !prefs_bbox ?
            SPItem::VISUAL_BBOX : SPItem::GEOMETRIC_BBOX );
        Geom::OptRect r = item->documentBounds(bbox_type);
        if (r) {
            w = scale_units*r->dimensions()[Geom::X];
            h = scale_units*r->dimensions()[Geom::Y];
            x0 = scale_units*r->min()[Geom::X];
            y0 = scale_units*r->min()[Geom::Y];
            center = scale_units*desktop->dt2doc(item->getCenter());

            sp_repr_set_svg_double(obj_repr, "inkscape:tile-cx", center[Geom::X]);
            sp_repr_set_svg_double(obj_repr, "inkscape:tile-cy", center[Geom::Y]);
            sp_repr_set_svg_double(obj_repr, "inkscape:tile-w", w);
            sp_repr_set_svg_double(obj_repr, "inkscape:tile-h", h);
            sp_repr_set_svg_double(obj_repr, "inkscape:tile-x0", x0);
            sp_repr_set_svg_double(obj_repr, "inkscape:tile-y0", y0);
        } else {
            center = Geom::Point(0, 0);
            w = h = 0;
            x0 = y0 = 0;
        }
    }

    Geom::Point cur(0, 0);
    Geom::Rect bbox_original (Geom::Point (x0, y0), Geom::Point (x0 + w, y0 + h));
    double perimeter_original = (w + h)/4;

    /**
     * Settings carried based on the type of pattern and button
     * selected
     */

    if(pattern_rotational == 1) {
    	jmax = 1;
    	imax = noOfRepeat;

    	shiftx_per_i = -1;
    	shifty_per_i = 0;
    	shiftx_per_j = 0;
    	shifty_per_j = 0;
    	shiftx_rand  = 0;
    	shifty_rand  = 0;
    	shiftx_exp   = 1;
    	shifty_exp   = 1;
    	shiftx_alternate =    0;
    	shifty_alternate =    0;
    	shiftx_cumulate  =    0;
    	shifty_cumulate  =    0;
    	shiftx_excludew  =    0;
    	shifty_excludeh  =    0;

    	scalex_per_i = 0;
    	scaley_per_i = 0;
    	scalex_per_j = 0;
    	scaley_per_j = 0;
    	scalex_rand  = 0;
    	scaley_rand  = 0;
    	scalex_exp   =  1;
    	scaley_exp   =  1;
    	scalex_log   =  0;
    	scaley_log   =  0;
    	scalex_alternate = 0;
    	scaley_alternate = 0;
    	scalex_cumulate  = 0;
    	scaley_cumulate  = 0;

    	rotate_per_i = 360/imax;
    	rotate_per_j = 0;
    	rotate_rand = 0;
    	rotate_alternatei = 0;
    	rotate_alternatej = 0;
    	rotate_cumulatei = 0;
    	rotate_cumulatej = 0;

    }

    if(pattern_shift == 1) {

    	shiftx_rand  = 0;
    	shifty_rand  = 0;
    	shiftx_exp   = 1;
    	shifty_exp   = 1;
    	shiftx_cumulate  =	0;
    	shifty_cumulate  =  0;
    	shiftx_excludew  =  0;
    	shifty_excludeh  =  0;

    	scalex_per_i = 0;
    	scaley_per_i = 0;
    	scalex_per_j = 0;
    	scaley_per_j = 0;
    	scalex_rand  = 0;
    	scaley_rand  = 0;
    	scalex_exp   =  1;
    	scaley_exp   =  1;
    	scalex_log   =  0;
    	scaley_log   =  0;
    	scalex_alternate = 0;
    	scaley_alternate = 0;
    	scalex_cumulate  = 0;
    	scaley_cumulate  = 0;

    	rotate_per_i = 0;
    	rotate_per_j = 0;
    	rotate_rand = 0;
    	rotate_alternatei = 0;
    	rotate_alternatej = 0;
    	rotate_cumulatei = 0;
    	rotate_cumulatej = 0;

    	switch(pattern_shift_type) {
    	case 0:
    		{

    			double internalRowCalculation = gapBetweenRow/h;
    			double internalColCalculation = gapBetweenCols/w;
    			shiftx_per_i = internalColCalculation;
    			shifty_per_i = 0;
    			shiftx_per_j = 0;
    			shifty_per_j = internalRowCalculation;
    			shiftx_alternate = 0;
    			shifty_alternate = 0;
    		}
    		break;
    	case 1:
    		{
    			double internalCalculation = gapBetweenCols/w;
    			shiftx_per_i = internalCalculation;
    			shifty_per_i = 0.5;
    			shiftx_per_j = 0;
    			shifty_per_j = 0;
    			shiftx_alternate =    1;
    			shifty_alternate =    0;
    		}
    		break;
    	case 2:
    		{
    			double internalCalculation = gapBetweenRow/h;
    			shiftx_per_i = 0;
    			shifty_per_i = 0;
    			shiftx_per_j = 0.5;
    			shifty_per_j = internalCalculation;
    			shiftx_alternate = 0;
    			shifty_alternate = 1;
    		}
    		break;
    	case 3:
    		{
    			double internalRowCalculation = gapBetweenRow/h;
    			double internalColCalculation = gapBetweenCols/w;
    			shiftx_per_i = internalColCalculation;
    			shifty_per_i = 0;
    			shiftx_per_j = 0;
    			shifty_per_j = internalRowCalculation;
    			shiftx_alternate = 0;
    			shifty_alternate = 0;

    		}
    		break;
    	default:
    		break;
    	}
    }

    // The integers i and j are reserved for tile column and row.
    // The doubles x and y are used for coordinates
    for (int i = 0;
         fillrect?
             (fabs(cur[Geom::X]) < fillwidth && i < 200) // prevent "freezing" with too large fillrect, arbitrarily limit rows
             : (i < imax);
         i ++) {
        for (int j = 0;
             fillrect?
                 (fabs(cur[Geom::Y]) < fillheight && j < 200) // prevent "freezing" with too large fillrect, arbitrarily limit cols
                 : (j < jmax);
             j ++) {

            // Note: We create a clone at 0,0 too, right over the original, in case our clones are colored

            // Get transform from symmetry, shift, scale, rotation
        	Geom::Affine orig_t = dicclonetiler_get_transform (type, i, j, center[Geom::X], center[Geom::Y], w, h,
            												   shiftx_per_i,     shifty_per_i,
															   shiftx_per_j,     shifty_per_j,
															   shiftx_rand,      shifty_rand,
															   shiftx_exp,       shifty_exp,
															   shiftx_alternate, shifty_alternate,
															   shiftx_cumulate,  shifty_cumulate,
															   shiftx_excludew,  shifty_excludeh,
															   scalex_per_i,     scaley_per_i,
															   scalex_per_j,     scaley_per_j,
															   scalex_rand,      scaley_rand,
															   scalex_exp,       scaley_exp,
															   scalex_log,       scaley_log,
															   scalex_alternate, scaley_alternate,
															   scalex_cumulate,  scaley_cumulate,
															   rotate_per_i,     rotate_per_j,
															   rotate_rand,
															   rotate_alternatei, rotate_alternatej,
															   rotate_cumulatei,  rotate_cumulatej      );

            Geom::Affine parent_transform = (((SPItem*)item->parent)->i2doc_affine())*(item->document->getRoot()->c2p.inverse());
            Geom::Affine t = parent_transform*orig_t*parent_transform.inverse();
            cur = center * t - center;
            if (fillrect) {
                if ((cur[Geom::X] > fillwidth) || (cur[Geom::Y] > fillheight)) { // off limits
                    continue;
                }
            }

            gchar color_string[32]; *color_string = 0;

            // Color tab
            if (!initial_color.empty()) {
                guint32 rgba = sp_svg_read_color (initial_color.data(), 0x000000ff);
                float hsl[3];
                sp_color_rgb_to_hsl_floatv (hsl, SP_RGBA32_R_F(rgba), SP_RGBA32_G_F(rgba), SP_RGBA32_B_F(rgba));

                double eff_i = (color_alternatei? (i%2) : (i));
                double eff_j = (color_alternatej? (j%2) : (j));

                hsl[0] += hue_per_i * eff_i + hue_per_j * eff_j + hue_rand * g_random_double_range (-1, 1);
                double notused;
                hsl[0] = modf( hsl[0], &notused ); // Restrict to 0-1
                hsl[1] += saturation_per_i * eff_i + saturation_per_j * eff_j + saturation_rand * g_random_double_range (-1, 1);
                hsl[1] = CLAMP (hsl[1], 0, 1);
                hsl[2] += lightness_per_i * eff_i + lightness_per_j * eff_j + lightness_rand * g_random_double_range (-1, 1);
                hsl[2] = CLAMP (hsl[2], 0, 1);

                float rgb[3];
                sp_color_hsl_to_rgb_floatv (rgb, hsl[0], hsl[1], hsl[2]);
                sp_svg_write_color(color_string, sizeof(color_string), SP_RGBA32_F_COMPOSE(rgb[0], rgb[1], rgb[2], 1.0));
            }

            // Blur
            double blur = 0.0;
            {
            int eff_i = (blur_alternatei? (i%2) : (i));
            int eff_j = (blur_alternatej? (j%2) : (j));
            blur =  (blur_per_i * eff_i + blur_per_j * eff_j + blur_rand * g_random_double_range (-1, 1));
            blur = CLAMP (blur, 0, 1);
            }

            // Opacity
            double opacity = 1.0;
            {
            int eff_i = (opacity_alternatei? (i%2) : (i));
            int eff_j = (opacity_alternatej? (j%2) : (j));
            opacity = 1 - (opacity_per_i * eff_i + opacity_per_j * eff_j + opacity_rand * g_random_double_range (-1, 1));
            opacity = CLAMP (opacity, 0, 1);
            }

            // Trace tab
            if (dotrace) {
                Geom::Rect bbox_t = transform_rect (bbox_original, t*Geom::Scale(1.0/scale_units));

                guint32 rgba = clonetiler_trace_pick (bbox_t);
                float r = SP_RGBA32_R_F(rgba);
                float g = SP_RGBA32_G_F(rgba);
                float b = SP_RGBA32_B_F(rgba);
                float a = SP_RGBA32_A_F(rgba);

                float hsl[3];
                sp_color_rgb_to_hsl_floatv (hsl, r, g, b);

                gdouble val = 0;
                switch (pick) {
                case DIC_PICK_COLOR:
                    val = 1 - hsl[2]; // inverse lightness; to match other picks where black = max
                    break;
                case DIC_PICK_OPACITY:
                    val = a;
                    break;
                case DIC_PICK_R:
                    val = r;
                    break;
                case DIC_PICK_G:
                    val = g;
                    break;
                case DIC_PICK_B:
                    val = b;
                    break;
                case DIC_PICK_H:
                    val = hsl[0];
                    break;
                case DIC_PICK_S:
                    val = hsl[1];
                    break;
                case DIC_PICK_L:
                    val = 1 - hsl[2];
                    break;
                default:
                    break;
                }

                if (rand_picked > 0) {
                    val = randomize01 (val, rand_picked);
                    r = randomize01 (r, rand_picked);
                    g = randomize01 (g, rand_picked);
                    b = randomize01 (b, rand_picked);
                }

                if (gamma_picked != 0) {
                    double power;
                    if (gamma_picked > 0)
                        power = 1/(1 + fabs(gamma_picked));
                    else
                        power = 1 + fabs(gamma_picked);

                    val = pow (val, power);
                    r = pow (r, power);
                    g = pow (g, power);
                    b = pow (b, power);
                }

                if (invert_picked) {
                    val = 1 - val;
                    r = 1 - r;
                    g = 1 - g;
                    b = 1 - b;
                }

                val = CLAMP (val, 0, 1);
                r = CLAMP (r, 0, 1);
                g = CLAMP (g, 0, 1);
                b = CLAMP (b, 0, 1);

                // recompose tweaked color
                rgba = SP_RGBA32_F_COMPOSE(r, g, b, a);

                if (pick_to_presence) {
                    if (g_random_double_range (0, 1) > val) {
                        continue; // skip!
                    }
                }
                if (pick_to_size) {
                    t = parent_transform * Geom::Translate(-center[Geom::X], -center[Geom::Y])
                    * Geom::Scale (val, val) * Geom::Translate(center[Geom::X], center[Geom::Y])
                    * parent_transform.inverse() * t;
                }
                if (pick_to_opacity) {
                    opacity *= val;
                }
                if (pick_to_color) {
                    sp_svg_write_color(color_string, sizeof(color_string), rgba);
                }
            }

            if (opacity < 1e-6) { // invisibly transparent, skip
                continue;
            }

            if (fabs(t[0]) + fabs (t[1]) + fabs(t[2]) + fabs(t[3]) < 1e-6) { // too small, skip
                continue;
            }

            if(pattern_shift_type  == 3 && pattern_rotational == 0) {

            	if(i%2 == 0) {
            		if(j%2 == 1) {
            		}
            		else {
            			Inkscape::XML::Node *clone = obj_repr->document()->createElement("svg:use");
            			clone->setAttribute("x", "0");
            			clone->setAttribute("y", "0");
            			clone->setAttribute("inkscape:tiled-clone-of", id_href);
            			clone->setAttribute("xlink:href", id_href);

            			Geom::Point new_center;
            			bool center_set = false;
            			if (obj_repr->attribute("inkscape:transform-center-x") || obj_repr->attribute("inkscape:transform-center-y")) {
            				new_center = scale_units*desktop->dt2doc(item->getCenter()) * orig_t;
            				center_set = true;
            			}

            			gchar *affinestr=sp_svg_transform_write(t);
            			clone->setAttribute("transform", affinestr);
            			g_free(affinestr);

            			if (opacity < 1.0) {
            				sp_repr_set_css_double(clone, "opacity", opacity);
            			}

            			if (*color_string) {
            				clone->setAttribute("fill", color_string);
            				clone->setAttribute("stroke", color_string);
            			}

    			        // add the new clone to the top of the original's parent
            			parent->getRepr()->appendChild(clone);

            			if (blur > 0.0) {
            				SPObject *clone_object = desktop->getDocument()->getObjectByRepr(clone);
            				double perimeter = perimeter_original * t.descrim();
            				double radius = blur * perimeter;
            				// this is necessary for all newly added clones to have correct bboxes,
            				// otherwise filters won't work:
            				desktop->getDocument()->ensureUpToDate();
            				// it's hard to figure out exact width/height of the tile without having an object
            				// that we can take bbox of; however here we only need a lower bound so that blur
            				// margins are not too small, and the perimeter should work
            				SPFilter *constructed = new_filter_gaussian_blur(desktop->getDocument(), radius, t.descrim(), t.expansionX(), t.expansionY(), perimeter, perimeter);
            				sp_style_set_property_url (clone_object, "filter", constructed, false);
            			}

            			if (center_set) {
            				SPObject *clone_object = desktop->getDocument()->getObjectByRepr(clone);
            				SPItem *item = dynamic_cast<SPItem *>(clone_object);
            				if (clone_object && item) {
            					clone_object->requestDisplayUpdate(SP_OBJECT_MODIFIED_FLAG);
            					item->setCenter(desktop->doc2dt(new_center));
            					clone_object->updateRepr();
            				}
            			}

    			Inkscape::GC::release(clone);
    		}

    	}
    	else if(i%2 == 1) {
    		if(j%2==0) {
    		}
    		else {
    			Inkscape::XML::Node *clone = obj_repr->document()->createElement("svg:use");
    			clone->setAttribute("x", "0");
    			clone->setAttribute("y", "0");
    			clone->setAttribute("inkscape:tiled-clone-of", id_href);
    			clone->setAttribute("xlink:href", id_href);

    			Geom::Point new_center;
    			bool center_set = false;
    			if (obj_repr->attribute("inkscape:transform-center-x") || obj_repr->attribute("inkscape:transform-center-y")) {
    				new_center = scale_units*desktop->dt2doc(item->getCenter()) * orig_t;
    				center_set = true;
    			}

    			gchar *affinestr=sp_svg_transform_write(t);
    			clone->setAttribute("transform", affinestr);
    			g_free(affinestr);

    			if (opacity < 1.0) {
    				sp_repr_set_css_double(clone, "opacity", opacity);
    			}

    			if (*color_string) {
    				clone->setAttribute("fill", color_string);
    				clone->setAttribute("stroke", color_string);
    			}

    			// add the new clone to the top of the original's parent
    			parent->getRepr()->appendChild(clone);

    			if (blur > 0.0) {
    				SPObject *clone_object = desktop->getDocument()->getObjectByRepr(clone);
    				double perimeter = perimeter_original * t.descrim();
    				double radius = blur * perimeter;
    				// this is necessary for all newly added clones to have correct bboxes,
    				// otherwise filters won't work:
    				desktop->getDocument()->ensureUpToDate();
    				// it's hard to figure out exact width/height of the tile without having an object
    				// that we can take bbox of; however here we only need a lower bound so that blur
    				// margins are not too small, and the perimeter should work
    				SPFilter *constructed = new_filter_gaussian_blur(desktop->getDocument(), radius, t.descrim(), t.expansionX(), t.expansionY(), perimeter, perimeter);
    				sp_style_set_property_url (clone_object, "filter", constructed, false);
    			}

    			if (center_set) {
    				SPObject *clone_object = desktop->getDocument()->getObjectByRepr(clone);
    				SPItem *item = dynamic_cast<SPItem *>(clone_object);
    				if (clone_object && item) {
    					clone_object->requestDisplayUpdate(SP_OBJECT_MODIFIED_FLAG);
    					item->setCenter(desktop->doc2dt(new_center));
    					clone_object->updateRepr();
    				}
    			}

    			Inkscape::GC::release(clone);
    		}
    	}

    }

    else if(pattern_shift_type == 0 || pattern_shift_type == 1 ||  pattern_shift_type == 2 || pattern_rotational == 1) {
    	// Create the clone
    	Inkscape::XML::Node *clone = obj_repr->document()->createElement("svg:use");
    	clone->setAttribute("x", "0");
    	clone->setAttribute("y", "0");
    	clone->setAttribute("inkscape:tiled-clone-of", id_href);
    	clone->setAttribute("xlink:href", id_href);


    	Geom::Point new_center;
    	bool center_set = false;
    	if (obj_repr->attribute("inkscape:transform-center-x") || obj_repr->attribute("inkscape:transform-center-y")) {
    		new_center = scale_units*desktop->dt2doc(item->getCenter()) * orig_t;
    		center_set = true;
    	}

    	gchar *affinestr=sp_svg_transform_write(t);
    	clone->setAttribute("transform", affinestr);
    	g_free(affinestr);

    	if (opacity < 1.0) {
    		sp_repr_set_css_double(clone, "opacity", opacity);
    	}

    	if (*color_string) {
    		clone->setAttribute("fill", color_string);
    		clone->setAttribute("stroke", color_string);
    	}

    	// add the new clone to the top of the original's parent
    	parent->getRepr()->appendChild(clone);

    	if (blur > 0.0) {
    		SPObject *clone_object = desktop->getDocument()->getObjectByRepr(clone);
    		double perimeter = perimeter_original * t.descrim();
    		double radius = blur * perimeter;
    		//this is necessary for all newly added clones to have correct bboxes,
    		// otherwise filters won't work:
    		desktop->getDocument()->ensureUpToDate();
    		// it's hard to figure out exact width/height of the tile without having an object
    		// that we can take bbox of; however here we only need a lower bound so that blur
    		// margins are not too small, and the perimeter should work
    		//SPFilter *constructed = new_filter_gaussian_blur(desktop->getDocument(), radius, t.descrim(), t.expansionX(), t.expansionY(), perimeter, perimeter);
    		//sp_style_set_property_url (clone_object, "filter", constructed, false);
    	}

    	if (center_set) {
    		SPObject *clone_object = desktop->getDocument()->getObjectByRepr(clone);
    		SPItem *item = dynamic_cast<SPItem *>(clone_object);
    		if (clone_object && item) {
    			clone_object->requestDisplayUpdate(SP_OBJECT_MODIFIED_FLAG);
    			item->setCenter(desktop->doc2dt(new_center));
    			clone_object->updateRepr();
    		}
    	}

    	Inkscape::GC::release(clone);
    }
        }
        cur[Geom::Y] = 0;
    }

    if (dotrace) {
    	clonetiler_trace_finish ();
    }

    clonetiler_change_selection(selection,dlg);

    desktop->clearWaitingCursor();

    DocumentUndo::done(desktop->getDocument(), SP_VERB_DIALOG_DIC_CLONETILER,
    					_("Create DIC Tiled Clones"));
}

/**
 *  Function to maintain updated info based on the
 *  tab selection done i.e DIC Shift tab/ DIC Rotation tab
 */

#if WITH_GTKMM_3_0
void DicCloneTiler::_ondicSwitchPage(Gtk::Widget * /*page*/, guint pagenum)
#else
void DicCloneTiler::_ondicSwitchPage(GtkNotebookPage * /*page*/, guint pagenum)
#endif
{
	_dicsavePagePref(pagenum);

}

void DicCloneTiler::_dicsavePagePref(guint page_num)
{
    // remember the current page
    Inkscape::Preferences *prefs = Inkscape::Preferences::get();
    prefs->setInt("/dialogs/dicclonetiler/page", page_num);

    if(page_num == 0) {

    	pattern_shift = 1;
    	pattern_rotational = 0;

    }

    if(page_num == 1) {

    	pattern_rotational = 1;
    	pattern_shift = 0;

    }
}

/**
 * Function to keep count of number of repeats
 * selected on DIC Rotation tab
 */

void DicCloneTiler::dicclonetiler_number_of_repeat_changed(GtkAdjustment *adj, gpointer data)
{

	noOfRepeat = gtk_adjustment_get_value (adj);

}

/**
 * This code generates the DIC Shift tab buttons interface
 */

GtkWidget * DicCloneTiler::dicclonetiler_table_shift_pattern_buttons(int values)
{

#if GTK_CHECK_VERSION(3,0,0)
    GtkWidget *table = gtk_grid_new();
    gtk_grid_set_row_spacing(GTK_GRID(table), 6);
    gtk_grid_set_column_spacing(GTK_GRID(table), 8);
#else
    GtkWidget *table = gtk_table_new (values + 2, 5, FALSE);
    gtk_table_set_row_spacings (GTK_TABLE (table), 20);
    gtk_table_set_col_spacings (GTK_TABLE (table), 0);
#endif

    gtk_container_set_border_width (GTK_CONTAINER (table), 0);

    {
#if GTK_CHECK_VERSION(3,0,0)
    	GtkWidget *hb = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
    	gtk_box_set_homogeneous(GTK_BOX(hb), FALSE);
#else
    	GtkWidget *hb = gtk_hbox_new (FALSE, 0);
#endif
    	imageRegularTiling = gtk_image_new_from_file ("./share/regular_tiling.png");
    	radioRegularTiling = gtk_radio_button_new_with_label (NULL, "");
    	gtk_toggle_button_set_mode(GTK_TOGGLE_BUTTON(radioRegularTiling),FALSE);
    	gtk_button_set_image (GTK_BUTTON (radioRegularTiling), imageRegularTiling);
    	gtk_box_pack_start (GTK_BOX (hb), radioRegularTiling, TRUE, TRUE, 0);
    	group = gtk_radio_button_group (GTK_RADIO_BUTTON (radioRegularTiling));
    	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (radioRegularTiling), TRUE);
    	gtk_widget_show (imageRegularTiling);
    	gtk_widget_show (radioRegularTiling);
    	gtk_widget_set_tooltip_text (radioRegularTiling, _("Regular Tiling pattern will be created"));
    	g_signal_connect (G_OBJECT (radioRegularTiling), "toggled",
    					  G_CALLBACK (dicclonetiler_shift_pattern_radiobutton_handler), GINT_TO_POINTER(DIC_PATTERN_REGULAR));

    	clonetiler_table_attach (table, hb, 0, 1, 1);
    }

    {
#if GTK_CHECK_VERSION(3,0,0)
    	GtkWidget *hb = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
    	gtk_box_set_homogeneous(GTK_BOX(hb), FALSE);
#else
    	GtkWidget *hb = gtk_hbox_new (FALSE, 0);
#endif
    	imageRowHalfDrop = gtk_image_new_from_file ("./share/row_half_drop.png");
    	radioRowHalfDrop = gtk_radio_button_new_with_label (group, "");
    	gtk_toggle_button_set_mode(GTK_TOGGLE_BUTTON(radioRowHalfDrop),FALSE);
    	gtk_button_set_image (GTK_BUTTON (radioRowHalfDrop), imageRowHalfDrop);
    	gtk_box_pack_start (GTK_BOX (hb), radioRowHalfDrop, TRUE, TRUE, 0);
    	group = gtk_radio_button_group (GTK_RADIO_BUTTON (radioRowHalfDrop));
    	gtk_widget_show (imageRowHalfDrop);
    	gtk_widget_show (radioRowHalfDrop);
    	gtk_widget_set_tooltip_text (radioRowHalfDrop, _("Row Half drop pattern will be created"));
    	g_signal_connect (G_OBJECT (radioRowHalfDrop), "toggled",
    					  G_CALLBACK (dicclonetiler_shift_pattern_radiobutton_handler), GINT_TO_POINTER(DIC_PATTERN_ROW_HALF_DROP));

    	clonetiler_table_attach (table, hb, 0, 1, 2);
    }

    {
#if GTK_CHECK_VERSION(3,0,0)
    	GtkWidget *hb = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
    	gtk_box_set_homogeneous(GTK_BOX(hb), FALSE);
#else
    	GtkWidget *hb = gtk_hbox_new (FALSE, 0);
#endif
		imageColumnHalfDrop = gtk_image_new_from_file ("./share/col_half_drop.png");
    	radioColHalfDrop = gtk_radio_button_new_with_label (group, "");
    	gtk_toggle_button_set_mode(GTK_TOGGLE_BUTTON(radioColHalfDrop),FALSE);
    	gtk_button_set_image (GTK_BUTTON (radioColHalfDrop), imageColumnHalfDrop);
    	gtk_box_pack_start (GTK_BOX (hb), radioColHalfDrop, TRUE, TRUE, 0);
    	group = gtk_radio_button_group (GTK_RADIO_BUTTON (radioColHalfDrop));
    	gtk_widget_show (imageColumnHalfDrop);
    	gtk_widget_show (radioColHalfDrop);
    	gtk_widget_set_tooltip_text (radioColHalfDrop, _("Col Half drop pattern will be created"));
    	g_signal_connect (G_OBJECT (radioColHalfDrop), "toggled",
    					  G_CALLBACK (dicclonetiler_shift_pattern_radiobutton_handler), GINT_TO_POINTER(DIC_PATTERN_COL_HALF_DROP));

    	clonetiler_table_attach (table, hb, 0, 1, 3);
    }

    {
#if GTK_CHECK_VERSION(3,0,0)
    	GtkWidget *hb = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
    	gtk_box_set_homogeneous(GTK_BOX(hb), FALSE);
#else
    	GtkWidget *hb = gtk_hbox_new (FALSE, 0);
#endif
    	imageFullDrop = gtk_image_new_from_file ("./share/full_drop.png");
    	radioFullDrop = gtk_radio_button_new_with_label (group, "");
    	gtk_toggle_button_set_mode(GTK_TOGGLE_BUTTON(radioFullDrop),FALSE);
    	gtk_button_set_image (GTK_BUTTON (radioFullDrop), imageFullDrop);
    	gtk_box_pack_start (GTK_BOX (hb), radioFullDrop, TRUE, TRUE, 0);
    	group = gtk_radio_button_group (GTK_RADIO_BUTTON (radioFullDrop));
    	gtk_widget_show (imageFullDrop);
    	gtk_widget_show (radioFullDrop);
    	gtk_widget_set_tooltip_text (radioFullDrop, _("Full drop pattern will be created"));
    	g_signal_connect (G_OBJECT (radioFullDrop), "toggled",
    					  G_CALLBACK (dicclonetiler_shift_pattern_radiobutton_handler), GINT_TO_POINTER(DIC_PATTERN_FULL_DROP));

    	clonetiler_table_attach (table, hb, 0, 1, 4);
    }

    return table;
}

/**
 * Function to modify selection based on the type of
 * DIC Shift tab button selected
 */

void DicCloneTiler::dicclonetiler_shift_pattern_radiobutton_handler(GtkToggleButton *tb, gpointer data)
{

	if(gtk_toggle_button_get_active(tb)) {
    	guint value = GPOINTER_TO_INT (data);
    	pattern_shift_type = GPOINTER_TO_INT (data);

    	switch(value) {
    	case 0:
    		gtk_widget_set_sensitive(lRow, true);
    		gtk_widget_set_sensitive(GTK_WIDGET(rowDistanceSpinButton->gobj()), true);
    		gtk_widget_set_sensitive(GTK_WIDGET(unit_menu->gobj()),true);
    		gtk_widget_set_sensitive(lCol, true);
    		gtk_widget_set_sensitive(GTK_WIDGET(colDistanceSpinButton->gobj()), true);
    		gtk_widget_set_sensitive(GTK_WIDGET(unit_col_menu->gobj()),true);
    		break;
    	case 1:
    		gtk_widget_set_sensitive(lRow, false);
    		gtk_widget_set_sensitive(GTK_WIDGET(rowDistanceSpinButton->gobj()), false);
    		gtk_widget_set_sensitive(GTK_WIDGET(unit_menu->gobj()),false);
    		gtk_widget_set_sensitive(lCol, true);
    		gtk_widget_set_sensitive(GTK_WIDGET(colDistanceSpinButton->gobj()), true);
    		gtk_widget_set_sensitive(GTK_WIDGET(unit_col_menu->gobj()),true);
    		break;
    	case 2:
    		gtk_widget_set_sensitive(lRow, true);
    		gtk_widget_set_sensitive(GTK_WIDGET(rowDistanceSpinButton->gobj()), true);
    		gtk_widget_set_sensitive(GTK_WIDGET(unit_menu->gobj()),true);
    		gtk_widget_set_sensitive(lCol, false);
    		gtk_widget_set_sensitive(GTK_WIDGET(colDistanceSpinButton->gobj()), false);
    		gtk_widget_set_sensitive(GTK_WIDGET(unit_col_menu->gobj()),false);
    		break;
    	case 3:
    		gtk_widget_set_sensitive(lRow, true);
    		gtk_widget_set_sensitive(GTK_WIDGET(rowDistanceSpinButton->gobj()), true);
    		gtk_widget_set_sensitive(GTK_WIDGET(unit_menu->gobj()),true);
    		gtk_widget_set_sensitive(lCol, true);
    		gtk_widget_set_sensitive(GTK_WIDGET(colDistanceSpinButton->gobj()), true);
    		gtk_widget_set_sensitive(GTK_WIDGET(unit_col_menu->gobj()),true);
    		break;
    	default:
    		break;
    	}
    }

}

Geom::Affine DicCloneTiler::dicclonetiler_get_transform(
    // symmetry group
    int type,

    // row, column
    int i, int j,

    // center, width, height of the tile
    double cx, double cy,
    double w,  double h,

    // values from the dialog:
    // Shift
    double shiftx_per_i,      double shifty_per_i,
    double shiftx_per_j,      double shifty_per_j,
    double shiftx_rand,       double shifty_rand,
    double shiftx_exp,        double shifty_exp,
    int    shiftx_alternate,  int    shifty_alternate,
    int    shiftx_cumulate,   int    shifty_cumulate,
    int    shiftx_excludew,   int    shifty_excludeh,

    // Scale
    double scalex_per_i,      double scaley_per_i,
    double scalex_per_j,      double scaley_per_j,
    double scalex_rand,       double scaley_rand,
    double scalex_exp,        double scaley_exp,
    double scalex_log,        double scaley_log,
    int    scalex_alternate,  int    scaley_alternate,
    int    scalex_cumulate,   int    scaley_cumulate,

    // Rotation
    double rotate_per_i,      double rotate_per_j,
    double rotate_rand,
    int    rotate_alternatei, int    rotate_alternatej,
    int    rotate_cumulatei,  int    rotate_cumulatej
    )
{

    // Shift (in units of tile width or height) -------------
    double delta_shifti = 0.0;
    double delta_shiftj = 0.0;

    if( shiftx_alternate ) {
        delta_shifti = (double)(i%2);
    } else {
        if( shiftx_cumulate ) {  // Should the delta shifts be cumulative (i.e. 1, 1+2, 1+2+3, ...)
            delta_shifti = (double)(i*i);
        } else {
            delta_shifti = (double)i;
        }
    }

    if( shifty_alternate ) {
        delta_shiftj = (double)(j%2);
    } else {
        if( shifty_cumulate ) {
            delta_shiftj = (double)(j*j);
        } else {
            delta_shiftj = (double)j;
        }
    }

    // Random shift, only calculate if non-zero.
    double delta_shiftx_rand = 0.0;
    double delta_shifty_rand = 0.0;
    if( shiftx_rand != 0.0 ) delta_shiftx_rand = shiftx_rand * g_random_double_range (-1, 1);
    if( shifty_rand != 0.0 ) delta_shifty_rand = shifty_rand * g_random_double_range (-1, 1);


    if(pattern_shift_type == 1) {

    	shiftx_per_i = shiftx_per_i * i;
    	delta_shifti = 1;
    }

    if(pattern_shift_type  == 2) {

    	if(j % 2 == 0) {
    		delta_shiftj = 0;
    	}
    	if(j % 2  == 1) {
    		delta_shiftj = 1;
    	}
    }
    // Delta shift (units of tile width/height)
    double di = shiftx_per_i * delta_shifti  + shiftx_per_j * delta_shiftj + delta_shiftx_rand;

    if(pattern_shift_type == 1) {

    	if(i % 2 == 0) {
    	    	delta_shifti = 0;
    	 }
    	 if(i%2 == 1) {
    		 delta_shifti = 1;
    	 }
    }

    if(pattern_shift_type  == 2) {

    	shifty_per_j = shifty_per_j * j;
    	delta_shiftj = 1;
    }

    double dj = shifty_per_i * delta_shifti  + shifty_per_j * delta_shiftj + delta_shifty_rand;

    // Shift in actual x and y, used below
    double dx = w * di;
    double dy = h * dj;

    double shifti = di;
    double shiftj = dj;

    // Include tile width and height in shift if required
    if( !shiftx_excludew ) shifti += i;
    if( !shifty_excludeh ) shiftj += j;

    // Add exponential shift if necessary
    double shifti_sign = (shifti > 0.0) ? 1.0 : -1.0;
    shifti = shifti_sign * pow(fabs(shifti), shiftx_exp);
    double shiftj_sign = (shiftj > 0.0) ? 1.0 : -1.0;
    shiftj = shiftj_sign * pow(fabs(shiftj), shifty_exp);

    // Final shift
    Geom::Affine rect_translate (Geom::Translate (w * shifti, h * shiftj));

    // Rotation (in degrees) ------------
    double delta_rotationi = 0.0;
    double delta_rotationj = 0.0;

    if( rotate_alternatei ) {
        delta_rotationi = (double)(i%2);
    } else {
        if( rotate_cumulatei ) {
            delta_rotationi = (double)(i*i + i)/2.0;
        } else {
            delta_rotationi = (double)i;
        }
    }

    if( rotate_alternatej ) {
        delta_rotationj = (double)(j%2);
    } else {
        if( rotate_cumulatej ) {
            delta_rotationj = (double)(j*j + j)/2.0;
        } else {
            delta_rotationj = (double)j;
        }
    }

    double delta_rotate_rand = 0.0;
    if( rotate_rand != 0.0 ) delta_rotate_rand = rotate_rand * 180.0 * g_random_double_range (-1, 1);

    double dr = rotate_per_i * delta_rotationi + rotate_per_j * delta_rotationj + delta_rotate_rand;

    // Scale (times the original) -----------
    double delta_scalei = 0.0;
    double delta_scalej = 0.0;

    if( scalex_alternate ) {
        delta_scalei = (double)(i%2);
    } else {
        if( scalex_cumulate ) {  // Should the delta scales be cumulative (i.e. 1, 1+2, 1+2+3, ...)
            delta_scalei = (double)(i*i + i)/2.0;
        } else {
            delta_scalei = (double)i;
        }
    }

    if( scaley_alternate ) {
        delta_scalej = (double)(j%2);
    } else {
        if( scaley_cumulate ) {
            delta_scalej = (double)(j*j + j)/2.0;
        } else {
            delta_scalej = (double)j;
        }
    }

    // Random scale, only calculate if non-zero.
    double delta_scalex_rand = 0.0;
    double delta_scaley_rand = 0.0;
    if( scalex_rand != 0.0 ) delta_scalex_rand = scalex_rand * g_random_double_range (-1, 1);
    if( scaley_rand != 0.0 ) delta_scaley_rand = scaley_rand * g_random_double_range (-1, 1);
    // But if random factors are same, scale x and y proportionally
    if( scalex_rand == scaley_rand ) delta_scalex_rand = delta_scaley_rand;

    // Total delta scale
    double scalex = 1.0 + scalex_per_i * delta_scalei  + scalex_per_j * delta_scalej + delta_scalex_rand;
    double scaley = 1.0 + scaley_per_i * delta_scalei  + scaley_per_j * delta_scalej + delta_scaley_rand;

    if( scalex < 0.0 ) scalex = 0.0;
    if( scaley < 0.0 ) scaley = 0.0;

    // Add exponential scale if necessary
    if ( scalex_exp != 1.0 ) scalex = pow( scalex, scalex_exp );
    if ( scaley_exp != 1.0 ) scaley = pow( scaley, scaley_exp );

    // Add logarithmic factor if necessary
    if ( scalex_log  > 0.0 ) scalex = pow( scalex_log, scalex - 1.0 );
    if ( scaley_log  > 0.0 ) scaley = pow( scaley_log, scaley - 1.0 );
    // Alternative using rotation angle
    //if ( scalex_log  != 1.0 ) scalex *= pow( scalex_log, M_PI*dr/180 );
    //if ( scaley_log  != 1.0 ) scaley *= pow( scaley_log, M_PI*dr/180 );


    // Calculate transformation matrices, translating back to "center of tile" (rotation center) before transforming
    Geom::Affine drot_c   = Geom::Translate(-cx, -cy) * Geom::Rotate (M_PI*dr/180)    * Geom::Translate(cx, cy);

    Geom::Affine dscale_c = Geom::Translate(-cx, -cy) * Geom::Scale (scalex, scaley)  * Geom::Translate(cx, cy);

    Geom::Affine d_s_r = dscale_c * drot_c;

    Geom::Affine rotate_180_c  = Geom::Translate(-cx, -cy) * Geom::Rotate (M_PI)      * Geom::Translate(cx, cy);

    Geom::Affine rotate_90_c   = Geom::Translate(-cx, -cy) * Geom::Rotate (-M_PI/2)   * Geom::Translate(cx, cy);
    Geom::Affine rotate_m90_c  = Geom::Translate(-cx, -cy) * Geom::Rotate ( M_PI/2)   * Geom::Translate(cx, cy);

    Geom::Affine rotate_120_c  = Geom::Translate(-cx, -cy) * Geom::Rotate (-2*M_PI/3) * Geom::Translate(cx, cy);
    Geom::Affine rotate_m120_c = Geom::Translate(-cx, -cy) * Geom::Rotate ( 2*M_PI/3) * Geom::Translate(cx, cy);

    Geom::Affine rotate_60_c   = Geom::Translate(-cx, -cy) * Geom::Rotate (-M_PI/3)   * Geom::Translate(cx, cy);
    Geom::Affine rotate_m60_c  = Geom::Translate(-cx, -cy) * Geom::Rotate ( M_PI/3)   * Geom::Translate(cx, cy);

    Geom::Affine flip_x        = Geom::Translate(-cx, -cy) * Geom::Scale (-1, 1)      * Geom::Translate(cx, cy);
    Geom::Affine flip_y        = Geom::Translate(-cx, -cy) * Geom::Scale (1, -1)      * Geom::Translate(cx, cy);


    // Create tile with required symmetry
    const double cos60 = cos(M_PI/3);
    const double sin60 = sin(M_PI/3);
    const double cos30 = cos(M_PI/6);
    const double sin30 = sin(M_PI/6);

    switch (type) {

    case DIC_TILE_P1:
        return d_s_r * rect_translate;
        break;

    case DIC_TILE_P2:
        if (i % 2 == 0) {
            return d_s_r * rect_translate;
        } else {
            return d_s_r * rotate_180_c * rect_translate;
        }
        break;

    case DIC_TILE_PM:
        if (i % 2 == 0) {
            return d_s_r * rect_translate;
        } else {
            return d_s_r * flip_x * rect_translate;
        }
        break;

    case DIC_TILE_PG:
        if (j % 2 == 0) {
            return d_s_r * rect_translate;
        } else {
            return d_s_r * flip_x * rect_translate;
        }
        break;

    case DIC_TILE_CM:
        if ((i + j) % 2 == 0) {
            return d_s_r * rect_translate;
        } else {
            return d_s_r * flip_x * rect_translate;
        }
        break;

    case DIC_TILE_PMM:
        if (j % 2 == 0) {
            if (i % 2 == 0) {
                return d_s_r * rect_translate;
            } else {
                return d_s_r * flip_x * rect_translate;
            }
        } else {
            if (i % 2 == 0) {
                return d_s_r * flip_y * rect_translate;
            } else {
                return d_s_r * flip_x * flip_y * rect_translate;
            }
        }
        break;

    case DIC_TILE_PMG:
        if (j % 2 == 0) {
            if (i % 2 == 0) {
                return d_s_r * rect_translate;
            } else {
                return d_s_r * rotate_180_c * rect_translate;
            }
        } else {
            if (i % 2 == 0) {
                return d_s_r * flip_y * rect_translate;
            } else {
                return d_s_r * rotate_180_c * flip_y * rect_translate;
            }
        }
        break;

    case DIC_TILE_PGG:
        if (j % 2 == 0) {
            if (i % 2 == 0) {
                return d_s_r * rect_translate;
            } else {
                return d_s_r * flip_y * rect_translate;
            }
        } else {
            if (i % 2 == 0) {
                return d_s_r * rotate_180_c * rect_translate;
            } else {
                return d_s_r * rotate_180_c * flip_y * rect_translate;
            }
        }
        break;

    case DIC_TILE_CMM:
        if (j % 4 == 0) {
            if (i % 2 == 0) {
                return d_s_r * rect_translate;
            } else {
                return d_s_r * flip_x * rect_translate;
            }
        } else if (j % 4 == 1) {
            if (i % 2 == 0) {
                return d_s_r * flip_y * rect_translate;
            } else {
                return d_s_r * flip_x * flip_y * rect_translate;
            }
        } else if (j % 4 == 2) {
            if (i % 2 == 1) {
                return d_s_r * rect_translate;
            } else {
                return d_s_r * flip_x * rect_translate;
            }
        } else {
            if (i % 2 == 1) {
                return d_s_r * flip_y * rect_translate;
            } else {
                return d_s_r * flip_x * flip_y * rect_translate;
            }
        }
        break;

    case DIC_TILE_P4:
    {
        Geom::Affine ori  (Geom::Translate ((w + h) * pow((i/2), shiftx_exp) + dx,  (h + w) * pow((j/2), shifty_exp) + dy));
        Geom::Affine dia1 (Geom::Translate (w/2 + h/2, -h/2 + w/2));
        Geom::Affine dia2 (Geom::Translate (-w/2 + h/2, h/2 + w/2));
        if (j % 2 == 0) {
            if (i % 2 == 0) {
                return d_s_r * ori;
            } else {
                return d_s_r * rotate_m90_c * dia1 * ori;
            }
        } else {
            if (i % 2 == 0) {
                return d_s_r * rotate_90_c * dia2 * ori;
            } else {
                return d_s_r * rotate_180_c * dia1 * dia2 * ori;
            }
        }
    }
    break;

    case DIC_TILE_P4M:
    {
        double max = MAX(w, h);
        Geom::Affine ori (Geom::Translate ((max + max) * pow((i/4), shiftx_exp) + dx,  (max + max) * pow((j/2), shifty_exp) + dy));
        Geom::Affine dia1 (Geom::Translate ( w/2 - h/2, h/2 - w/2));
        Geom::Affine dia2 (Geom::Translate (-h/2 + w/2, w/2 - h/2));
        if (j % 2 == 0) {
            if (i % 4 == 0) {
                return d_s_r * ori;
            } else if (i % 4 == 1) {
                return d_s_r * flip_y * rotate_m90_c * dia1 * ori;
            } else if (i % 4 == 2) {
                return d_s_r * rotate_m90_c * dia1 * Geom::Translate (h, 0) * ori;
            } else if (i % 4 == 3) {
                return d_s_r * flip_x * Geom::Translate (w, 0) * ori;
            }
        } else {
            if (i % 4 == 0) {
                return d_s_r * flip_y * Geom::Translate(0, h) * ori;
            } else if (i % 4 == 1) {
                return d_s_r * rotate_90_c * dia2 * Geom::Translate(0, h) * ori;
            } else if (i % 4 == 2) {
                return d_s_r * flip_y * rotate_90_c * dia2 * Geom::Translate(h, 0) * Geom::Translate(0, h) * ori;
            } else if (i % 4 == 3) {
                return d_s_r * flip_y * flip_x * Geom::Translate(w, 0) * Geom::Translate(0, h) * ori;
            }
        }
    }
    break;

    case DIC_TILE_P4G:
    {
        double max = MAX(w, h);
        Geom::Affine ori (Geom::Translate ((max + max) * pow((i/4), shiftx_exp) + dx,  (max + max) * pow(j, shifty_exp) + dy));
        Geom::Affine dia1 (Geom::Translate ( w/2 + h/2, h/2 - w/2));
        Geom::Affine dia2 (Geom::Translate (-h/2 + w/2, w/2 + h/2));
        if (((i/4) + j) % 2 == 0) {
            if (i % 4 == 0) {
                return d_s_r * ori;
            } else if (i % 4 == 1) {
                return d_s_r * rotate_m90_c * dia1 * ori;
            } else if (i % 4 == 2) {
                return d_s_r * rotate_90_c * dia2 * ori;
            } else if (i % 4 == 3) {
                return d_s_r * rotate_180_c * dia1 * dia2 * ori;
            }
        } else {
            if (i % 4 == 0) {
                return d_s_r * flip_y * Geom::Translate (0, h) * ori;
            } else if (i % 4 == 1) {
                return d_s_r * flip_y * rotate_m90_c * dia1 * Geom::Translate (-h, 0) * ori;
            } else if (i % 4 == 2) {
                return d_s_r * flip_y * rotate_90_c * dia2 * Geom::Translate (h, 0) * ori;
            } else if (i % 4 == 3) {
                return d_s_r * flip_x * Geom::Translate (w, 0) * ori;
            }
        }
    }
    break;

    case DIC_TILE_P3:
    {
        double width;
        double height;
        Geom::Affine dia1;
        Geom::Affine dia2;
        if (w > h) {
            width  = w + w * cos60;
            height = 2 * w * sin60;
            dia1 = Geom::Affine (Geom::Translate (w/2 + w/2 * cos60, -(w/2 * sin60)));
            dia2 = dia1 * Geom::Affine (Geom::Translate (0, 2 * (w/2 * sin60)));
        } else {
            width = h * cos (M_PI/6);
            height = h;
            dia1 = Geom::Affine (Geom::Translate (h/2 * cos30, -(h/2 * sin30)));
            dia2 = dia1 * Geom::Affine (Geom::Translate (0, h/2));
        }
        Geom::Affine ori (Geom::Translate (width * pow((2*(i/3) + j%2), shiftx_exp) + dx,  (height/2) * pow(j, shifty_exp) + dy));
        if (i % 3 == 0) {
            return d_s_r * ori;
        } else if (i % 3 == 1) {
            return d_s_r * rotate_m120_c * dia1 * ori;
        } else if (i % 3 == 2) {
            return d_s_r * rotate_120_c * dia2 * ori;
        }
    }
    break;

    case DIC_TILE_P31M:
    {
        Geom::Affine ori;
        Geom::Affine dia1;
        Geom::Affine dia2;
        Geom::Affine dia3;
        Geom::Affine dia4;
        if (w > h) {
            ori = Geom::Affine(Geom::Translate (w * pow((i/6) + 0.5*(j%2), shiftx_exp) + dx,  (w * cos30) * pow(j, shifty_exp) + dy));
            dia1 = Geom::Affine (Geom::Translate (0, h/2) * Geom::Translate (w/2, 0) * Geom::Translate (w/2 * cos60, -w/2 * sin60) * Geom::Translate (-h/2 * cos30, -h/2 * sin30) );
            dia2 = dia1 * Geom::Affine (Geom::Translate (h * cos30, h * sin30));
            dia3 = dia2 * Geom::Affine (Geom::Translate (0, 2 * (w/2 * sin60 - h/2 * sin30)));
            dia4 = dia3 * Geom::Affine (Geom::Translate (-h * cos30, h * sin30));
        } else {
            ori  = Geom::Affine (Geom::Translate (2*h * cos30  * pow((i/6 + 0.5*(j%2)), shiftx_exp) + dx,  (2*h - h * sin30) * pow(j, shifty_exp) + dy));
            dia1 = Geom::Affine (Geom::Translate (0, -h/2) * Geom::Translate (h/2 * cos30, h/2 * sin30));
            dia2 = dia1 * Geom::Affine (Geom::Translate (h * cos30, h * sin30));
            dia3 = dia2 * Geom::Affine (Geom::Translate (0, h/2));
            dia4 = dia3 * Geom::Affine (Geom::Translate (-h * cos30, h * sin30));
        }
        if (i % 6 == 0) {
            return d_s_r * ori;
        } else if (i % 6 == 1) {
            return d_s_r * flip_y * rotate_m120_c * dia1 * ori;
        } else if (i % 6 == 2) {
            return d_s_r * rotate_m120_c * dia2 * ori;
        } else if (i % 6 == 3) {
            return d_s_r * flip_y * rotate_120_c * dia3 * ori;
        } else if (i % 6 == 4) {
            return d_s_r * rotate_120_c * dia4 * ori;
        } else if (i % 6 == 5) {
            return d_s_r * flip_y * Geom::Translate(0, h) * ori;
        }
    }
    break;

    case DIC_TILE_P3M1:
    {
        double width;
        double height;
        Geom::Affine dia1;
        Geom::Affine dia2;
        Geom::Affine dia3;
        Geom::Affine dia4;
        if (w > h) {
            width = w + w * cos60;
            height = 2 * w * sin60;
            dia1 = Geom::Affine (Geom::Translate (0, h/2) * Geom::Translate (w/2, 0) * Geom::Translate (w/2 * cos60, -w/2 * sin60) * Geom::Translate (-h/2 * cos30, -h/2 * sin30) );
            dia2 = dia1 * Geom::Affine (Geom::Translate (h * cos30, h * sin30));
            dia3 = dia2 * Geom::Affine (Geom::Translate (0, 2 * (w/2 * sin60 - h/2 * sin30)));
            dia4 = dia3 * Geom::Affine (Geom::Translate (-h * cos30, h * sin30));
        } else {
            width = 2 * h * cos (M_PI/6);
            height = 2 * h;
            dia1 = Geom::Affine (Geom::Translate (0, -h/2) * Geom::Translate (h/2 * cos30, h/2 * sin30));
            dia2 = dia1 * Geom::Affine (Geom::Translate (h * cos30, h * sin30));
            dia3 = dia2 * Geom::Affine (Geom::Translate (0, h/2));
            dia4 = dia3 * Geom::Affine (Geom::Translate (-h * cos30, h * sin30));
        }
        Geom::Affine ori (Geom::Translate (width * pow((2*(i/6) + j%2), shiftx_exp) + dx,  (height/2) * pow(j, shifty_exp) + dy));
        if (i % 6 == 0) {
            return d_s_r * ori;
        } else if (i % 6 == 1) {
            return d_s_r * flip_y * rotate_m120_c * dia1 * ori;
        } else if (i % 6 == 2) {
            return d_s_r * rotate_m120_c * dia2 * ori;
        } else if (i % 6 == 3) {
            return d_s_r * flip_y * rotate_120_c * dia3 * ori;
        } else if (i % 6 == 4) {
            return d_s_r * rotate_120_c * dia4 * ori;
        } else if (i % 6 == 5) {
            return d_s_r * flip_y * Geom::Translate(0, h) * ori;
        }
    }
    break;

    case DIC_TILE_P6:
    {
        Geom::Affine ori;
        Geom::Affine dia1;
        Geom::Affine dia2;
        Geom::Affine dia3;
        Geom::Affine dia4;
        Geom::Affine dia5;
        if (w > h) {
            ori = Geom::Affine(Geom::Translate (w * pow((2*(i/6) + (j%2)), shiftx_exp) + dx,  (2*w * sin60) * pow(j, shifty_exp) + dy));
            dia1 = Geom::Affine (Geom::Translate (w/2 * cos60, -w/2 * sin60));
            dia2 = dia1 * Geom::Affine (Geom::Translate (w/2, 0));
            dia3 = dia2 * Geom::Affine (Geom::Translate (w/2 * cos60, w/2 * sin60));
            dia4 = dia3 * Geom::Affine (Geom::Translate (-w/2 * cos60, w/2 * sin60));
            dia5 = dia4 * Geom::Affine (Geom::Translate (-w/2, 0));
        } else {
            ori = Geom::Affine(Geom::Translate (2*h * cos30 * pow((i/6 + 0.5*(j%2)), shiftx_exp) + dx,  (h + h * sin30) * pow(j, shifty_exp) + dy));
            dia1 = Geom::Affine (Geom::Translate (-w/2, -h/2) * Geom::Translate (h/2 * cos30, -h/2 * sin30) * Geom::Translate (w/2 * cos60, w/2 * sin60));
            dia2 = dia1 * Geom::Affine (Geom::Translate (-w/2 * cos60, -w/2 * sin60) * Geom::Translate (h/2 * cos30, -h/2 * sin30) * Geom::Translate (h/2 * cos30, h/2 * sin30) * Geom::Translate (-w/2 * cos60, w/2 * sin60));
            dia3 = dia2 * Geom::Affine (Geom::Translate (w/2 * cos60, -w/2 * sin60) * Geom::Translate (h/2 * cos30, h/2 * sin30) * Geom::Translate (-w/2, h/2));
            dia4 = dia3 * dia1.inverse();
            dia5 = dia3 * dia2.inverse();
        }
        if (i % 6 == 0) {
            return d_s_r * ori;
        } else if (i % 6 == 1) {
            return d_s_r * rotate_m60_c * dia1 * ori;
        } else if (i % 6 == 2) {
            return d_s_r * rotate_m120_c * dia2 * ori;
        } else if (i % 6 == 3) {
            return d_s_r * rotate_180_c * dia3 * ori;
        } else if (i % 6 == 4) {
            return d_s_r * rotate_120_c * dia4 * ori;
        } else if (i % 6 == 5) {
            return d_s_r * rotate_60_c * dia5 * ori;
        }
    }
    break;

    case DIC_TILE_P6M:
    {

        Geom::Affine ori;
        Geom::Affine dia1, dia2, dia3, dia4, dia5, dia6, dia7, dia8, dia9, dia10;
        if (w > h) {
            ori = Geom::Affine(Geom::Translate (w * pow((2*(i/12) + (j%2)), shiftx_exp) + dx,  (2*w * sin60) * pow(j, shifty_exp) + dy));
            dia1 = Geom::Affine (Geom::Translate (w/2, h/2) * Geom::Translate (-w/2 * cos60, -w/2 * sin60) * Geom::Translate (-h/2 * cos30, h/2 * sin30));
            dia2 = dia1 * Geom::Affine (Geom::Translate (h * cos30, -h * sin30));
            dia3 = dia2 * Geom::Affine (Geom::Translate (-h/2 * cos30, h/2 * sin30) * Geom::Translate (w * cos60, 0) * Geom::Translate (-h/2 * cos30, -h/2 * sin30));
            dia4 = dia3 * Geom::Affine (Geom::Translate (h * cos30, h * sin30));
            dia5 = dia4 * Geom::Affine (Geom::Translate (-h/2 * cos30, -h/2 * sin30) * Geom::Translate (-w/2 * cos60, w/2 * sin60) * Geom::Translate (w/2, -h/2));
            dia6 = dia5 * Geom::Affine (Geom::Translate (0, h));
            dia7 = dia6 * dia1.inverse();
            dia8 = dia6 * dia2.inverse();
            dia9 = dia6 * dia3.inverse();
            dia10 = dia6 * dia4.inverse();
        } else {
            ori = Geom::Affine(Geom::Translate (4*h * cos30 * pow((i/12 + 0.5*(j%2)), shiftx_exp) + dx,  (2*h  + 2*h * sin30) * pow(j, shifty_exp) + dy));
            dia1 = Geom::Affine (Geom::Translate (-w/2, -h/2) * Geom::Translate (h/2 * cos30, -h/2 * sin30) * Geom::Translate (w/2 * cos60, w/2 * sin60));
            dia2 = dia1 * Geom::Affine (Geom::Translate (h * cos30, -h * sin30));
            dia3 = dia2 * Geom::Affine (Geom::Translate (-w/2 * cos60, -w/2 * sin60) * Geom::Translate (h * cos30, 0) * Geom::Translate (-w/2 * cos60, w/2 * sin60));
            dia4 = dia3 * Geom::Affine (Geom::Translate (h * cos30, h * sin30));
            dia5 = dia4 * Geom::Affine (Geom::Translate (w/2 * cos60, -w/2 * sin60) * Geom::Translate (h/2 * cos30, h/2 * sin30) * Geom::Translate (-w/2, h/2));
            dia6 = dia5 * Geom::Affine (Geom::Translate (0, h));
            dia7 = dia6 * dia1.inverse();
            dia8 = dia6 * dia2.inverse();
            dia9 = dia6 * dia3.inverse();
            dia10 = dia6 * dia4.inverse();
        }
        if (i % 12 == 0) {
            return d_s_r * ori;
        } else if (i % 12 == 1) {
            return d_s_r * flip_y * rotate_m60_c * dia1 * ori;
        } else if (i % 12 == 2) {
            return d_s_r * rotate_m60_c * dia2 * ori;
        } else if (i % 12 == 3) {
            return d_s_r * flip_y * rotate_m120_c * dia3 * ori;
        } else if (i % 12 == 4) {
            return d_s_r * rotate_m120_c * dia4 * ori;
        } else if (i % 12 == 5) {
            return d_s_r * flip_x * dia5 * ori;
        } else if (i % 12 == 6) {
            return d_s_r * flip_x * flip_y * dia6 * ori;
        } else if (i % 12 == 7) {
            return d_s_r * flip_y * rotate_120_c * dia7 * ori;
        } else if (i % 12 == 8) {
            return d_s_r * rotate_120_c * dia8 * ori;
        } else if (i % 12 == 9) {
            return d_s_r * flip_y * rotate_60_c * dia9 * ori;
        } else if (i % 12 == 10) {
            return d_s_r * rotate_60_c * dia10 * ori;
        } else if (i % 12 == 11) {
            return d_s_r * flip_y * Geom::Translate (0, h) * ori;
        }
    }
    break;

    default:
        break;
    }

    return Geom::identity();
}

}
}
}


/*
  Local Variables:
  mode:c++
  c-file-style:"stroustrup"
  c-file-offsets:((innamespace . 0)(inline-open . 0)(case-label . +))
  indent-tabs-mode:nil
  fill-column:99
  End:
*/
// vim: filetype=cpp:expandtab:shiftwidth=4:tabstop=8:softtabstop=4:fileencoding=utf-8:textwidth=99 :
