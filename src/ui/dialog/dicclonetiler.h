/*
 * dicclonetiler.h
 *
 *  Created on: 08-May-2019
 *      Author: Vishal Virmani
 */

#ifndef SRC_UI_DIALOG_DICCLONETILER_H_
#define SRC_UI_DIALOG_DICCLONETILER_H_

#include "ui/widget/panel.h"

#include "ui/dialog/desktop-tracker.h"
#include "ui/widget/color-picker.h"
#include "sp-root.h"

#include "ui/widget/unit-tracker.h"

#include <gtkmm/radiobutton.h>


namespace Inkscape {
namespace UI {

namespace Widget {
    class UnitMenu;
}

namespace Dialog {

class DicCloneTiler : public Widget::Panel {
public:
    DicCloneTiler();
    virtual ~DicCloneTiler();

    static DicCloneTiler &getInstance() { return *new DicCloneTiler(); }
    void show_page_trace();


protected:

    GtkWidget * clonetiler_new_tab(GtkWidget *nb, const gchar *label);
    GtkWidget * clonetiler_table_x_y_rand(int values);
    GtkWidget * clonetiler_spinbox(const char *tip, const char *attr, double lower, double upper, const gchar *suffix, bool exponent = false);
    GtkWidget * clonetiler_checkbox(const char *tip, const char *attr);
    void clonetiler_table_attach(GtkWidget *table, GtkWidget *widget, float align, int row, int col);

    static void clonetiler_symgroup_changed(GtkComboBox *cb, gpointer /*data*/);
    static void clonetiler_remove(GtkWidget */*widget*/, GtkWidget *dlg, bool do_undo = true);
    static void on_picker_color_changed(guint rgba);
    static void clonetiler_trace_hide_tiled_clones_recursively(SPObject *from);
    static void clonetiler_checkbox_toggled(GtkToggleButton *tb, gpointer *data);
    static void clonetiler_pick_switched(GtkToggleButton */*tb*/, gpointer data);
    static void clonetiler_do_pick_toggled(GtkToggleButton *tb, GtkWidget *dlg);
    static void clonetiler_pick_to(GtkToggleButton *tb, gpointer data);
    static void clonetiler_xy_changed(GtkAdjustment *adj, gpointer data);
    static void clonetiler_fill_width_changed(GtkAdjustment *adj, Inkscape::UI::Widget::UnitMenu *u);
    static void clonetiler_fill_height_changed(GtkAdjustment *adj, Inkscape::UI::Widget::UnitMenu *u);
    void clonetiler_unit_changed();
    static void clonetiler_switch_to_create(GtkToggleButton */*tb*/, GtkWidget *dlg);
    static void clonetiler_switch_to_fill(GtkToggleButton */*tb*/, GtkWidget *dlg);
    static void clonetiler_keep_bbox_toggled(GtkToggleButton *tb, gpointer /*data*/);
    static void clonetiler_apply(GtkWidget */*widget*/, GtkWidget *dlg);
    static void clonetiler_unclump(GtkWidget */*widget*/, void *);
    static void clonetiler_change_selection(Inkscape::Selection *selection, GtkWidget *dlg);
    static void clonetiler_external_change(GtkWidget *dlg);
    static void clonetiler_disconnect_gsignal(GObject *widget, gpointer source);
    static void clonetiler_reset(GtkWidget */*widget*/, GtkWidget *dlg);
    static guint clonetiler_number_of_clones(SPObject *obj);
    static void clonetiler_trace_setup(SPDocument *doc, gdouble zoom, SPItem *original);
    static guint32 clonetiler_trace_pick(Geom::Rect box);
    static void clonetiler_trace_finish();
    static bool clonetiler_is_a_clone_of(SPObject *tile, SPObject *obj);
    static Geom::Rect transform_rect(Geom::Rect const &r, Geom::Affine const &m);
    static double randomize01(double val, double rand);
    static void clonetiler_value_changed(GtkAdjustment *adj, gpointer data);
    static void clonetiler_reset_recursive(GtkWidget *w);

    static Geom::Affine clonetiler_get_transform(    // symmetry group
            int type,

            // row, column
            int i, int j,

            // center, width, height of the tile
            double cx, double cy,
            double w,  double h,

            // values from the dialog:
            // Shift
            double shiftx_per_i,      double shifty_per_i,
            double shiftx_per_j,      double shifty_per_j,
            double shiftx_rand,       double shifty_rand,
            double shiftx_exp,        double shifty_exp,
            int    shiftx_alternate,  int    shifty_alternate,
            int    shiftx_cumulate,   int    shifty_cumulate,
            int    shiftx_excludew,   int    shifty_excludeh,

            // Scale
            double scalex_per_i,      double scaley_per_i,
            double scalex_per_j,      double scaley_per_j,
            double scalex_rand,       double scaley_rand,
            double scalex_exp,        double scaley_exp,
            double scalex_log,        double scaley_log,
            int    scalex_alternate,  int    scaley_alternate,
            int    scalex_cumulate,   int    scaley_cumulate,

            // Rotation
            double rotate_per_i,      double rotate_per_j,
            double rotate_rand,
            int    rotate_alternatei, int    rotate_alternatej,
            int    rotate_cumulatei,  int    rotate_cumulatej
            );

    GtkWidget * dicclonetiler_table_shift_pattern_buttons(int values);
    GtkWidget * dicclonetiler_number_of_row_col_spinbox(const char *tip, const char *attr, int lower, int upper);
    GtkWidget * dicclonetiler_row_gap_spinbox();
    GtkWidget * dicclonetiler_col_gap_spinbox();

    static void dicclonetiler_number_of_row_col_changed(GtkAdjustment *adj, gpointer data);
    void dicclonetiler_unit_row_changed();
    void dicclonetiler_unit_col_changed();
    static void dicclonetiler_row_gap_value_changed(GtkAdjustment *adj, Inkscape::UI::Widget::UnitMenu *u);
    static void dicclonetiler_col_gap_value_changed(GtkAdjustment *adj, Inkscape::UI::Widget::UnitMenu *u);
    static void dicclonetiler_apply(GtkWidget */*widget*/, GtkWidget *dlg);
    void _dicsavePagePref(guint page_num);

#if WITH_GTKMM_3_0
    void _ondicSwitchPage(Gtk::Widget *page, guint pagenum);
#else
    void _ondicSwitchPage(GtkNotebookPage *page, guint pagenum);
#endif

    static void dicclonetiler_number_of_repeat_changed(GtkAdjustment *adj, gpointer data);
    static void dicclonetiler_shift_pattern_radiobutton_handler(GtkToggleButton *tb, gpointer data);

    static Geom::Affine dicclonetiler_get_transform(    // symmetry group
        		int type,

    			// row, column
    			int i, int j,

    			// center, width, height of the tile
    			double cx, double cy,
    			double w,  double h,

    			// values from the dialog:
    			// Shift
    			double shiftx_per_i,      double shifty_per_i,
    			double shiftx_per_j,      double shifty_per_j,
    			double shiftx_rand,       double shifty_rand,
    			double shiftx_exp,        double shifty_exp,
    			int    shiftx_alternate,  int    shifty_alternate,
    			int    shiftx_cumulate,   int    shifty_cumulate,
    			int    shiftx_excludew,   int    shifty_excludeh,

    			// Scale
    			double scalex_per_i,      double scaley_per_i,
    			double scalex_per_j,      double scaley_per_j,
    			double scalex_rand,       double scaley_rand,
    			double scalex_exp,        double scaley_exp,
    			double scalex_log,        double scaley_log,
    			int    scalex_alternate,  int    scaley_alternate,
    			int    scalex_cumulate,   int    scaley_cumulate,

    			// Rotation
    			double rotate_per_i,      double rotate_per_j,
    			double rotate_rand,
    			int    rotate_alternatei, int    rotate_alternatej,
    			int    rotate_cumulatei,  int    rotate_cumulatej
        );

private:
    DicCloneTiler(DicCloneTiler const &d);
    DicCloneTiler& operator=(DicCloneTiler const &d);

    GtkWidget *dlg;
    GtkWidget *nb;
    GtkWidget *b;
    SPDesktop *desktop;
    DesktopTracker deskTrack;
    Inkscape::UI::Widget::ColorPicker *color_picker;
    GtkSizeGroup* table_row_labels;

#if WITH_GTKMM_3_0
    Glib::RefPtr<Gtk::Adjustment> fill_width;
    Glib::RefPtr<Gtk::Adjustment> fill_height;
#else
    Gtk::Adjustment *fill_width;
    Gtk::Adjustment *fill_height;
#endif

    sigc::connection desktopChangeConn;
    sigc::connection selectChangedConn;
    sigc::connection externChangedConn;
    sigc::connection subselChangedConn;
    sigc::connection selectModifiedConn;
    sigc::connection color_changed_connection;
    sigc::connection unitChangedConn;

    /**
     * Can be invoked for setting the desktop. Currently not used.
     */
    void setDesktop(SPDesktop *desktop);

    /**
     * Is invoked by the desktop tracker when the desktop changes.
     */
    void setTargetDesktop(SPDesktop *desktop);

    /**
     * GtkWidgets to hold the iconic shift pattern images for DicClonetiler
     */

    GtkWidget *imageRegularTiling;
    GtkWidget *imageRowHalfDrop;
    GtkWidget *imageColumnHalfDrop;
    GtkWidget *imageFullDrop;

    /**
     * rowDistance and colDistance are meant to propagate the row gap and col gap if any needed
     * intAdjustmentRotaional is for the spin button functionality on the DIC Rotation tab
     */

#if WITH_GTKMM_3_0
    Glib::RefPtr<Gtk::Adjustment> rowDistance;
    Glib::RefPtr<Gtk::Adjustment> colDistance;
    Glib::RefPtr<Gtk::Adjustment> intAdjustmentRotational;
#else
    Gtk::Adjustment *rowDistance;
    Gtk::Adjustment *colDistance;
    Gtk::Adjustment *intAdjustmentRotational;
#endif

    sigc::connection unitColChangedConn;

    /**
     * GtkWidgets for Shift pattern radio buttons in DIC Shift tab
     */

    GtkWidget *radioRegularTiling;
    GtkWidget *radioRowHalfDrop;
    GtkWidget *radioColHalfDrop;
    GtkWidget *radioFullDrop;
    GSList *group;

};


enum {
    DIC_PICK_COLOR,
    DIC_PICK_OPACITY,
    DIC_PICK_R,
    DIC_PICK_G,
    DIC_PICK_B,
    DIC_PICK_H,
    DIC_PICK_S,
    DIC_PICK_L
};

enum {
    DIC_TILE_P1,
    DIC_TILE_P2,
    DIC_TILE_PM,
    DIC_TILE_PG,
    DIC_TILE_CM,
    DIC_TILE_PMM,
    DIC_TILE_PMG,
    DIC_TILE_PGG,
    DIC_TILE_CMM,
    DIC_TILE_P4,
    DIC_TILE_P4M,
    DIC_TILE_P4G,
    DIC_TILE_P3,
    DIC_TILE_P31M,
    DIC_TILE_P3M1,
    DIC_TILE_P6,
    DIC_TILE_P6M
};

enum {
    DIC_PATTERN_REGULAR,
	DIC_PATTERN_ROW_HALF_DROP,
	DIC_PATTERN_COL_HALF_DROP,
	DIC_PATTERN_FULL_DROP

};

} // namespace Dialog
} // namespace UI
} // namespace Inkscape


#endif    /* SRC_UI_DIALOG_DICCLONETILER_H_ */

